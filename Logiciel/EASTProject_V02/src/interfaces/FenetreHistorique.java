
package interfaces;

import java.util.ArrayList;

import javafx.scene.Parent;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import logiciels.ProjetFestival;
import logiciels.StandBoisson;
import logiciels.StandBoutique;
import logiciels.StandConversion;
import logiciels.StandNourriture;
import logiciels.StandPreventionSecours;
import logiciels.StandSanitaire;
import logiciels.StandScene;

public class FenetreHistorique {
	private ArrayList<StandBoisson> listeStandBoisson;
	private ArrayList<StandBoutique> listeStandBoutique;
	private ArrayList<StandConversion> listeStandConversion;
	private ArrayList<StandNourriture> listeStandNourriture;
	private ArrayList<StandPreventionSecours> listestandPreventionSecours;
	private ArrayList<StandSanitaire> listeStandSanitaire;
	private ArrayList<StandScene> listeStandScene;

	public FenetreHistorique() {
		listeStandBoisson = new ArrayList<StandBoisson>();
		listeStandBoutique = new ArrayList<StandBoutique>();
		listeStandConversion = new ArrayList<StandConversion>();
		listeStandNourriture = new ArrayList<StandNourriture>();
		listestandPreventionSecours = new ArrayList<StandPreventionSecours>();
		listeStandSanitaire = new ArrayList<StandSanitaire>();
		listeStandScene = new ArrayList<StandScene>();

		StandBoutique s1 = new StandBoutique("s1", "M", 3, 1, 2);
		StandBoutique s2 = new StandBoutique("s2", "M", 3, 1, 2);
		StandBoisson s3 = new StandBoisson("s3", "M", 3, 3, 3);
		StandBoisson s4 = new StandBoisson("s4", "M", 3, 3, 3);
		ProjetFestival.getCarte().ajoutStand(s1);
		ProjetFestival.getCarte().ajoutStand(s2);
		ProjetFestival.getCarte().ajoutStand(s3);
		ProjetFestival.getCarte().ajoutStand(s4);

		// TRI DES STANDS DANS ARRAYLIST
		if (ProjetFestival.getCarte().getStand().isEmpty()) {
			System.out.println("pas de stand dans carte");
		} else {
			for (int i = 0; i < ProjetFestival.getCarte().getStand().size(); i++) {

				if (ProjetFestival.getCarte().getStand().get(i).getNomClasse().equals("Stand Boisson")) {
					listeStandBoisson.add((StandBoisson) ProjetFestival.getCarte().getStand().get(i));
				} else if (ProjetFestival.getCarte().getStand().get(i).getNomClasse() == "Stand Boutique") {
					listeStandBoutique.add((StandBoutique) ProjetFestival.getCarte().getStand().get(i));
				} else if (ProjetFestival.getCarte().getStand().get(i).getNomClasse() == "Stand Conversion") {
					listeStandConversion.add((StandConversion) ProjetFestival.getCarte().getStand().get(i));
				} else if (ProjetFestival.getCarte().getStand().get(i).getNomClasse() == "Stand Nourriture") {
					listeStandNourriture.add((StandNourriture) ProjetFestival.getCarte().getStand().get(i));
				} else if (ProjetFestival.getCarte().getStand().get(i).getNomClasse() == "Stand Prévention Secours") {
					listestandPreventionSecours
							.add((StandPreventionSecours) ProjetFestival.getCarte().getStand().get(i));
				} else if (ProjetFestival.getCarte().getStand().get(i).getNomClasse() == "Stand Sanitaire") {
					listeStandSanitaire.add((StandSanitaire) ProjetFestival.getCarte().getStand().get(i));
				} else if (ProjetFestival.getCarte().getStand().get(i).getNomClasse() == "Stand Scene") {
					listeStandScene.add((StandScene) ProjetFestival.getCarte().getStand().get(i));
				}

			}
		}
	}

	public Parent affichage() {
		// DEBUT CREATION DE LA BASE DU TREEVIEW
		TreeView<String> treeView = new TreeView<String>();
		TreeItem<String> rootItem = new TreeItem<String>("Liste des Stands :");
		treeView.setPrefHeight(750);
		// FIN CREATION DE LA BASE DU TREEVIEW

		// DEBUT CREATION DES TREE ITEM EN FONCTION DES STAND INSTANCIES

		if (!this.listeStandBoisson.isEmpty()) {
			ArrayList<TreeItem<String>> standboissont = new ArrayList<TreeItem<String>>();
			for (int i = 0; i < this.listeStandBoisson.size(); i++) {
				standboissont.add(new TreeItem<String>(this.listeStandBoisson.get(i).toString()));
			}

			final TreeItem<String> standBoissonItem = new TreeItem<String>("Stand Boisson");
			for (int i = 0; i < standboissont.size(); i++) {
				standBoissonItem.getChildren().add(standboissont.get(i));
			}

			rootItem.getChildren().add(standBoissonItem);

		}
		if (!this.listeStandBoutique.isEmpty()) {

			ArrayList<TreeItem<String>> standboutiquet = new ArrayList<TreeItem<String>>();
			for (int i = 0; i < this.listeStandBoutique.size(); i++) {
				standboutiquet.add(new TreeItem<String>(this.listeStandBoutique.get(i).toString()));
			}
			final TreeItem<String> standBoutiqueItem = new TreeItem<String>("Stand Boutique");
			for (int i = 0; i < standboutiquet.size(); i++) {
				standBoutiqueItem.getChildren().add(standboutiquet.get(i));
			}
			rootItem.getChildren().add(standBoutiqueItem);
		}

		if (!this.listeStandConversion.isEmpty()) {

			ArrayList<TreeItem<String>> standconversiont = new ArrayList<TreeItem<String>>();
			for (int i = 0; i < this.listeStandConversion.size(); i++) {
				standconversiont.add(new TreeItem<String>(this.listeStandConversion.get(i).toString()));
			}
			final TreeItem<String> standConversionItem = new TreeItem<String>("Stand Conversion");
			for (int i = 0; i < standconversiont.size(); i++) {
				standConversionItem.getChildren().add(standconversiont.get(i));
			}
			rootItem.getChildren().add(standConversionItem);
		}

		if (!this.listeStandNourriture.isEmpty()) {

			ArrayList<TreeItem<String>> standnourrituret = new ArrayList<TreeItem<String>>();
			for (int i = 0; i < this.listeStandNourriture.size(); i++) {
				standnourrituret.add(new TreeItem<String>(this.listeStandNourriture.get(i).toString()));
			}
			final TreeItem<String> standNourritureItem = new TreeItem<String>("Stand Nourriture");
			for (int i = 0; i < standnourrituret.size(); i++) {
				standNourritureItem.getChildren().add(standnourrituret.get(i));
			}
			rootItem.getChildren().add(standNourritureItem);
		}

		if (!this.listestandPreventionSecours.isEmpty()) {

			ArrayList<TreeItem<String>> standprevt = new ArrayList<TreeItem<String>>();
			for (int i = 0; i < this.listestandPreventionSecours.size(); i++) {
				standprevt.add(new TreeItem<String>(this.listestandPreventionSecours.get(i).toString()));
			}
			final TreeItem<String> standPrevItem = new TreeItem<String>("Stand Prevention Secours");
			for (int i = 0; i < standprevt.size(); i++) {
				standPrevItem.getChildren().add(standprevt.get(i));
			}
			rootItem.getChildren().add(standPrevItem);
		}
		if (!this.listeStandSanitaire.isEmpty()) {

			ArrayList<TreeItem<String>> standsanitairet = new ArrayList<TreeItem<String>>();
			for (int i = 0; i < this.listeStandSanitaire.size(); i++) {
				standsanitairet.add(new TreeItem<String>(this.listeStandSanitaire.get(i).toString()));
			}
			final TreeItem<String> standSanitaireItem = new TreeItem<String>("Stand Sanitaire");
			for (int i = 0; i < standsanitairet.size(); i++) {
				standSanitaireItem.getChildren().add(standsanitairet.get(i));
			}
			rootItem.getChildren().add(standSanitaireItem);
		}
		if (!this.listeStandScene.isEmpty()) {

			ArrayList<TreeItem<String>> standscenet = new ArrayList<TreeItem<String>>();
			for (int i = 0; i < this.listeStandScene.size(); i++) {
				standscenet.add(new TreeItem<String>(this.listeStandScene.get(i).toString()));
			}
			final TreeItem<String> standSceneItem = new TreeItem<String>("Stand Scene");
			for (int i = 0; i < standscenet.size(); i++) {
				standSceneItem.getChildren().add(standscenet.get(i));
			}
			standSceneItem.setExpanded(true);
			rootItem.getChildren().add(standSceneItem);
		}

		// FIN CREATION DES TREE ITEM EN FONCTION DES STAND INSTANCIES

		// DEBUT DES ELEMENTS DE BASE DE FONCTIONNEMENT
		treeView.setRoot(rootItem);

		return treeView;
	}

}
