package logiciels;

import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Representation implements Serializable{
	private LocalDate dateRepresentation;
	private LocalTime horaireRepresentaion;
	private LocalTime horaireDeFin;
	private long duree;
	private Artiste artiste;

	// Constructeur
	public Representation(LocalDate date, LocalTime horaire, Artiste a, long d) {
		this.dateRepresentation = date;
		this.duree = d;
		this.horaireRepresentaion = horaire;

		horaireDeFin = LocalTime.of(horaireRepresentaion.getHour(), horaireRepresentaion.getMinute())
				.plus(Duration.ofMinutes(d));

		artiste = a;
	}

	// GETTERS ET SETTERS
	public LocalDate getDateRepresentaion() {
		return dateRepresentation;
	}

	public LocalDateTime getDateetHoraireRepresentation() {
		return LocalDateTime.of(dateRepresentation, horaireRepresentaion);
	}

	public LocalDateTime getDateetHoraireFinRepresentation() {
		return LocalDateTime.of(dateRepresentation, horaireDeFin);
	}

	public Artiste getArtiste() {
		return artiste;
	}

	public void setHoraire(LocalDate date) {
		this.dateRepresentation = date;
	}

	public LocalTime getHoraireRepresentaion() {
		return horaireRepresentaion;
	}

	public LocalTime getHoraireDeFin() {
		return horaireDeFin;
	}

	public float getDuree() {
		return duree;
	}

	public void setDuree(long duree) {
		this.duree = duree;
	}

	@Override
	public String toString() {

		return "Date : " + dateRepresentation + "  Horaires : " + horaireRepresentaion + "h-" + horaireDeFin + "h"
				+ "  Duree : " + duree + "min" + "  Artiste : " + artiste;
	}

	// CALCULER LE COUT
	public float calculerCout(float f1, float f2) {
		return 0;
	}

}