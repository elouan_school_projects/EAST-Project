package interfaces;



import java.io.FileNotFoundException;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class CreationArtiste extends Stage{
	
	private GridPane 			saisie 				= new GridPane();
	private VBox 				labels_gauche 		= new VBox();
	private GridPane 			creer_scene 		= new GridPane();
	private ListView<String> 	liste_scene 		= new ListView<String>();
	private HBox 				boutons_bot 		= new HBox();
	private VBox 				poub				= new VBox();
	private HBox  				image_titre			= new HBox();
	
	private Label   			nom					= new Label("Nom : ");
	private Label   			cachet				= new Label("Cachet : ");
	private Label   			genre				= new Label("Genre : ");
	private Label   			scene				= new Label("Scenes : ");
	private Label 				liste_scenes		= new Label("Liste des sc�nes de l'Artiste");
	
	private TextField 			nom_s				= new TextField();
	private TextField 			cachet_s			= new TextField();
	private TextField 			genre_s				= new TextField();
	private TextField 			horaire				= new TextField();
	private TextField 			dur�e				= new TextField();
	
	private ComboBox<String> 	scenes_dispo		= new ComboBox<String>();
	
	private Button 				ajouter				= new Button("Ajouter");
	private Button 				creer				= new Button("Creer");
	private Button 				annuler				= new Button("Annuler");

	
	
	public CreationArtiste() throws FileNotFoundException
	{
		this.setTitle("Creation d'artiste");
		this.setResizable(false);
		Scene laScene = new Scene(affichage(), 550,400);
		this.setScene(laScene);
		this.sizeToScene();
	}
	public Parent affichage() throws FileNotFoundException
	{
		VBox root = new VBox();
		//Labels a gauche
		
		labels_gauche.getChildren().addAll(nom,cachet,genre,scene);
		labels_gauche.setSpacing(25);
		saisie.add(labels_gauche, 0, 0);
	
		//textfields droite
		creer_scene.add(nom_s, 0, 0,3,1);
		creer_scene.add(cachet_s, 0, 1,3,1);
		creer_scene.add(genre_s, 0, 2,3,1);
		creer_scene.add(scenes_dispo, 0, 3,2,1);
		creer_scene.add(horaire, 0, 4);
		creer_scene.add(dur�e, 1,4);
		creer_scene.add(ajouter, 2, 3,1,2);
		creer_scene.setHgap(10);
		creer_scene.setVgap(15);
		
		horaire.setPromptText("Horaire");
		dur�e.setPromptText("Dur�e (Minutes)");
		
		saisie.add(creer_scene, 1, 0);
		saisie.setHgap(90);
		scenes_dispo.setMaxWidth(Double.MAX_VALUE);
		
		//listview scenes
		liste_scene.setPrefHeight(90);
		liste_scene.getItems().add("Item 1");
		liste_scene.getItems().add("Item 2");
		liste_scene.getItems().add("Item 3");
		
		//Boutons Bot
		boutons_bot.getChildren().addAll(creer,annuler);
		boutons_bot.setAlignment(Pos.BASELINE_CENTER);
		boutons_bot.setSpacing(60);
		
		creer.setMaxWidth(200);
		annuler.setMaxWidth(200);
		HBox.setHgrow(creer,Priority.ALWAYS);
		HBox.setHgrow(annuler,Priority.ALWAYS);
		
		String url = "https://image.ibb.co/k6EqO8/54158.png" ;
		Image image = new Image(url);
		ImageView imageView = new ImageView(image);
		poub.setSpacing(2);
		
		//HBox pour le titre de la liste et l'image de poubelle
		image_titre.getChildren().addAll(imageView,liste_scenes);
		image_titre.setSpacing(170);
		
		poub.getChildren().addAll(image_titre,liste_scene);
		
		
		root.setSpacing(20);
		root.getChildren().addAll(saisie,poub,boutons_bot);
		root.setPadding(new Insets(20));
		
		//GESTION DE LA LISTVIEW pour enlever un item selectionner
	    imageView.setOnMouseClicked(new EventHandler<MouseEvent>() 
	    {
	      @Override public void handle(MouseEvent arg0) {
	        final int selectedIdx = liste_scene.getSelectionModel().getSelectedIndex();
	        if (selectedIdx != -1) {
	          
	 
	          final int newSelectedIdx =
	            (selectedIdx == liste_scene.getItems().size() - 1)
	               ? selectedIdx - 1
	               : selectedIdx;
	 
	          liste_scene.getItems().remove(selectedIdx);
	          
	          liste_scene.getSelectionModel().select(newSelectedIdx);
	        }
	      }
	    });
		
		
		return root;
	}
}
