package interfaces;



import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import logiciels.Carte;
import logiciels.Stand;


public class FenetreComptabilite{
	private TableView<Stand> 		tableau 			= new TableView<Stand>();
	private Label 					txtBudgetInitial 	= new Label();
	private Label 					txtDepenses 		= new Label("D�penses : �");
	private Label 					txtResultat 		= new Label("R�sultat : �");
	private VBox 					infos 				= new VBox();
	private float 					budgetInitial 		= 0;
	private float 					sommeDepenses 		= 0;
	private float 					resultat 			= 0;
	private ObservableList<Stand> 	donnees 			= FXCollections.observableArrayList();
	private Carte 					carte;


	public FenetreComptabilite() {
		txtBudgetInitial.setText("Budget initial : "+budgetInitial+" �");
		tableau.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		tableau.setEditable(false);
	}

	@SuppressWarnings("unchecked")
	Parent affichage(Carte c) {
		AnchorPane groot = new AnchorPane();
		carte = c;


		//CREATION DES COLONNES STAND ET COUT
		TableColumn<Stand, String> stand = new TableColumn<Stand, String>("Stand");
		stand.setCellValueFactory(
				new PropertyValueFactory<Stand, String>("nom"));
		TableColumn<Stand, Float> couts = new TableColumn<Stand, Float>("Co�t");
		couts.setCellValueFactory(
				new PropertyValueFactory<Stand, Float>("cout"));

		donnees.addAll(carte.getStand());

		tableau.setItems(donnees);
		tableau.getColumns().addAll(stand, couts);
		tableau.minWidth(Double.MAX_VALUE);


		//CREATION ET PLACEMENT DES INFOS BUDGETAIRES EN DESSOUS DU TABLEAU
		infos.getChildren().addAll(txtBudgetInitial, txtDepenses, txtResultat);
		VBox.setMargin(txtDepenses, new Insets(5, 10, 5, 150));
		VBox.setMargin(txtBudgetInitial, new Insets(5, 10, 5, 150));
		VBox.setMargin(txtResultat, new Insets(5, 10, 5, 150));

		//PLACEMENT DU TABLEAU
		AnchorPane.setTopAnchor(tableau, 10.0);
		AnchorPane.setLeftAnchor(tableau, 10.0);
		AnchorPane.setRightAnchor(tableau, 25.0);


		AnchorPane.setTopAnchor(infos, 450.0);
		AnchorPane.setLeftAnchor(infos, 250.0);
		AnchorPane.setRightAnchor(infos, 250.0);

		//AJOUT CSS : TAILLE, BORDURE...
		infos.setStyle("-fx-font-size : 30px;"
				+ "-fx-border-width: 1px;" + 
				"-fx-border-style: solid;" + 
				"-fx-border-color: lightgrey");


		groot.getChildren().addAll(tableau, infos);

		return groot;
	}

	public void setBudget(float budget) {
		budgetInitial = budget;
		txtBudgetInitial.setText("Budget initial : "+budgetInitial+" �");
	}


	public void actuCompta() {
		for(int i=0;i<carte.getStand().size();i++) {
			if(!donnees.contains(carte.getStand().get(i))) {
				donnees.add(carte.getStand().get(i));
			}
		}
		for(int i=0;i<donnees.size();i++) {
			sommeDepenses += donnees.get(i).getCout();
		}
		txtDepenses.setText("D�penses : "+sommeDepenses+" �");
		resultat = budgetInitial - sommeDepenses;
		txtResultat.setText("R�sultat : "+resultat+" �");
	}

}
