package logiciels;

public class StandConversion extends Stand {

	public StandConversion(String n, String taille, float hauteur, float longueur, float largeur) {
		super(n, taille, hauteur, longueur, largeur);

	}

	// METHODE DE CALCUL DU COUT
	@Override
	public float calculCout() {
		return 0;
	}

	// METHODES AFFICHAGE
	@Override
	public String toString() {
		return super.toString();
	}

	// METHODE OBTENTION DU NOM DE LA CLASSE
	@Override
	public String getNomClasse() {
		return "Stand Conversion";
	}

}
