package logiciels;

import java.util.Date;

public class Representation {
	private Date horaire;
	private float duree;

	// Constructeur
	public Representation(Date horaire, float duree) {
		this.horaire = horaire;
		this.duree = duree;
	}

	// GETTERS ET SETTERS
	public Date getHoraire() {
		return horaire;
	}

	public void setHoraire(Date horaire) {
		this.horaire = horaire;
	}

	public float getDuree() {
		return duree;
	}

	public void setDuree(float duree) {
		this.duree = duree;
	}

	// CALCULER LE COUT
	public float calculerCout(float f1, float f2) {
		return 0;
	}

}
