package logiciels;

import java.io.Serializable;
import java.util.ArrayList;

public class Carte implements Serializable {
	private double taille;
	private String emplacement;
	private ArrayList<Stand> listeStands;

	// CONSTRUCTEUR
	public Carte(double taille, String emplacement) {
		this.taille = taille;
		this.emplacement = emplacement;
		this.listeStands = new ArrayList<Stand>();
	}

	// GETTERS ET SETTERS
	public double getTaille() {
		return taille;
	}

	public void setTaille(double taille) {
		this.taille = taille;
	}

	public String getEmplacement() {
		return emplacement;
	}

	public void setEmplacement(String emplacement) {
		this.emplacement = emplacement;
	}

	public ArrayList<Stand> getStand() {
		return listeStands;
	}

	// AJOUTER UN STAND A LA CARTE
	public void ajoutStand(Stand s) {
		listeStands.add(s);
	}

	// MODIFIER UN STAND DE LA CARTE
	public void editerStand(Stand s) {
		int i = 0;
		boolean trouve = false;
		while ((i <= listeStands.size()) && (trouve == false)) {
			if (listeStands.get(i).equals(s)) {
				trouve = true;
			} else {
				i++;
			}
		}
		if (trouve) {
			listeStands.set(i, s);
		} else {
			System.out.println("Erreur : le stand n'existe pas dans la liste");
		}
	}

	// SUPPRIMMER UN STAND DE LA CARTE
	public void supprimmerStand(Stand s) {
		if (listeStands.contains(s)) {
			listeStands.remove(s);
		} else {
			System.out.println("Erreur : le stand n'existe pas dans la liste");
		}
	}

}
