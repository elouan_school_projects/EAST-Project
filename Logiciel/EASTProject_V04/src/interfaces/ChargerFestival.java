package interfaces;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import logiciels.ProjetFestival;

public class ChargerFestival extends Stage {
	
	private TableView<ProjetFestival> listeProjet = new TableView<ProjetFestival>();
	private Label txtCharger = new Label("Charger un projet :");
	private Button charger = new Button("Charger");
	private Button annuler = new Button("Annuler");
	private Button parcourir = new Button("Parcourir...");
	private ObservableList<ProjetFestival> donnees = FXCollections.observableArrayList();
	
	
	public ChargerFestival() throws ClassNotFoundException {
		this.setTitle("Charger un festival");
		this.setX(520);
		this.setY(300);
		this.setResizable(false);
		this.initModality(Modality.APPLICATION_MODAL);
		listeProjet.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
		
		
		Scene laScene = new Scene(affichage(), 500, 365);
		this.setScene(laScene);
		this.sizeToScene();
	}
	
	@SuppressWarnings("unchecked")
	Parent affichage() throws ClassNotFoundException {
		AnchorPane root = new AnchorPane();
		
		this.listerProjet();
		
		listeProjet.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		
		TableColumn<ProjetFestival, String> nomProjet = new TableColumn<ProjetFestival, String>("Nom");
		nomProjet.setCellValueFactory(
				new PropertyValueFactory<ProjetFestival, String>("nom"));
		TableColumn<ProjetFestival, String> path = new TableColumn<ProjetFestival, String>("Chemin");
		path.setCellValueFactory(
				new PropertyValueFactory<ProjetFestival, String>("path"));
		path.setResizable(true);
		
		charger.setPrefWidth(100);
		annuler.setPrefWidth(100);
		parcourir.setPrefWidth(100);
		
		AnchorPane.setTopAnchor(txtCharger, 10.0);
		AnchorPane.setLeftAnchor(txtCharger, 10.0);
		AnchorPane.setTopAnchor(listeProjet, 30.0);
		AnchorPane.setRightAnchor(listeProjet, 130.0);
		AnchorPane.setLeftAnchor(listeProjet, 10.0);
		AnchorPane.setBottomAnchor(listeProjet, 10.0);
		AnchorPane.setTopAnchor(parcourir, 20.0);
		AnchorPane.setRightAnchor(parcourir, 10.0);
		AnchorPane.setBottomAnchor(charger, 50.0);
		AnchorPane.setRightAnchor(charger, 10.0);
		AnchorPane.setBottomAnchor(annuler, 10.0);
		AnchorPane.setRightAnchor(annuler, 10.0);
		root.getChildren().addAll(txtCharger, charger, parcourir, annuler, listeProjet);
		
		listeProjet.setItems(donnees);
		listeProjet.getColumns().addAll(nomProjet, path);
		
		listeProjet.setOnMouseClicked(e -> {
			if(e.getClickCount() == 2) {
				this.charger(listeProjet.getSelectionModel().getSelectedItem());
			}
		});
		
		charger.disableProperty().bind(listeProjet.getSelectionModel().selectedItemProperty().isNull());
		
		charger.setOnAction(e -> {
			this.charger(listeProjet.getSelectionModel().getSelectedItem());
		});
		
		parcourir.setOnAction(e -> {
			FileChooser selecteurFichier = new FileChooser();
			File fichier = selecteurFichier.showOpenDialog(this);
			ProjetFestival projet;
			if(fichier != null) {
				try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(fichier))) {
					projet = (ProjetFestival) in.readObject();
					projet.setPath(fichier.getAbsolutePath());
					donnees.add(projet);
					
	            } catch (Exception exc) {
	                exc.printStackTrace();
	                }
				}
		});
		
		annuler.setOnAction(e -> {
			MenuPrincipal fen = new MenuPrincipal();
			fen.show();
			close();
		});
		
		
		return root;
	}
	
	public void charger(ProjetFestival projet) {
		FenetrePrincipale fen;
		try {
			FileInputStream fichierEntrant = new FileInputStream(projet.getPath());
			ObjectInputStream charger = new ObjectInputStream(fichierEntrant);
			projet = (ProjetFestival) charger.readObject();
			MenuPrincipal.projet = projet;
			fen = new FenetrePrincipale();
			charger.close();
			fichierEntrant.close();
		}catch(IOException e) {
			e.printStackTrace();
			return;
		}catch(ClassNotFoundException c) {
			System.out.println("classe projet introuvable");
			c.printStackTrace();
			return;
		}
		fen.show();
		close();
	}
	
	public void listerProjet() throws ClassNotFoundException {
		DirectoryStream<java.nio.file.Path> stream = null;
	      try{
	          java.nio.file.Path dir = Paths.get("sauvegardes/");
	    	  stream = Files.newDirectoryStream(dir, "*.ser");
	    	   for (java.nio.file.Path p: stream) {
	    		   ProjetFestival projet;
	    		   
	    		   FileInputStream fichierEntrant = new FileInputStream(p.toString());
	    		   ObjectInputStream charger = new ObjectInputStream(fichierEntrant);
	    		   projet = (ProjetFestival) charger.readObject();
	    		   donnees.add(projet);
	    		   charger.close();
		       }
		   } catch (IOException ex) {
		       ex.printStackTrace();
		   }finally{
			   try {
				stream.close();
			} catch (IOException e1) {
				e1.printStackTrace();
				}
			   }
	}
	
}
