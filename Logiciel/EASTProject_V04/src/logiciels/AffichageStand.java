package logiciels;

import java.io.Serializable;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

public class AffichageStand implements Serializable {
	private double coordX;
	private double coordY;

	private String url;
	private float largeur;
	private float longueur;
	private double stackWidth;
	private double stackHeight;

	public AffichageStand(double coordX, double coordY, String url, float largeur, float longueur, StackPane stack) {
		this.coordX = coordX;
		this.coordY = coordY;
		this.url = url;
		this.largeur = largeur;
		this.longueur = longueur;
		this.stackWidth = stack.getWidth();
		this.stackHeight = stack.getHeight();
	}

	public double getCoordX() {
		return coordX;
	}

	public void setCoordX(double coordX) {
		this.coordX = coordX;
	}

	public double getCoordY() {
		return coordY;
	}

	public void setCoordY(double coordY) {
		this.coordY = coordY;
	}

	public float getLargeur() {
		return largeur;
	}

	public void setLargeur(float largeur) {
		this.largeur = largeur;
	}

	public float getLongueur() {
		return longueur;
	}

	public void setLongueur(float longueur) {
		this.longueur = longueur;
	}

	public ImageView getImage() {
		ImageView image = new ImageView(
				new Image(getClass().getResource(url).toExternalForm(), largeur, longueur, false, false));
		image.setTranslateX(coordX - stackWidth / 2);
		image.setTranslateY(coordY - stackHeight / 2);
		return image;
	}

}
