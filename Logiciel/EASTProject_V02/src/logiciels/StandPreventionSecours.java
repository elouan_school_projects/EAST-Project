package logiciels;

import java.util.ArrayList;

public class StandPreventionSecours extends Stand {
	private int nbPers;
	private float coutPers;
	private ArrayList<ElementPreventionSecours> listeElementsPreventionSecour;

	// CONSTRUCTEUR
	public StandPreventionSecours(String n, String taille, float hauteur, float longueur, float largeur) {
		super(n, taille, hauteur, longueur, largeur);
		this.nbPers = 0;
		this.coutPers = 0;
		this.listeElementsPreventionSecour = new ArrayList<ElementPreventionSecours>();
	}

	// GETTERS ET SETTERS
	public float getCoutPers() {
		return coutPers;
	}

	public void setCoutPers(float coutPers) {
		this.coutPers = coutPers;
	}

	// AJOUTER UN ARTICLE DE PREVENTION AU STAND
	public void ajoutElementPreventionSecour(ElementPreventionSecours p) {
		listeElementsPreventionSecour.add(p);
	}

	// MODIFIER UN ARTICLE DE PREVENTION DU STAND
	public void editerElementPreventionSecour(ElementPreventionSecours p) {
		int i = 0;
		boolean trouve = false;
		while ((i <= listeElementsPreventionSecour.size()) && (trouve == false)) {
			if (listeElementsPreventionSecour.get(i).equals(p)) {
				trouve = true;
			} else {
				i++;
			}
		}
		if (trouve) {
			listeElementsPreventionSecour.set(i, p);
		} else {
			System.out.println("Erreur : l'article de pr�vention " + p + " n'existe pas dans la liste");
		}
	}

	// SUPPRIMMER UN ARTICLE DE PREVENTION DU STAND
	public void supprimmerElementPreventionSecour(ElementPreventionSecours p) {
		if (listeElementsPreventionSecour.contains(p)) {
			listeElementsPreventionSecour.remove(p);
		} else {
			System.out.println("Erreur : l'article de pr�vention" + p + " n'existe pas dans la liste");
		}
	}

	// METHODE DE CALCUL DU COUT
	@Override
	public float calculCout() {
		float coutTotal = 0;

		if (listeElementsPreventionSecour.isEmpty()) {
			System.out.println("Erreur : pas d'article de pr�vention assign� � ce stand");
		} else {
			for (int i = 0; i < listeElementsPreventionSecour.size(); i++) {
				coutTotal += listeElementsPreventionSecour.get(i).calculCout();
			}
		}
		return coutTotal + nbPers * coutPers;
	}

	// METHODE OBTENTION DU NOM DE LA CLASSE
	@Override
	public String getNomClasse() {
		return "Stand Pr�vention Secours";
	}

}
