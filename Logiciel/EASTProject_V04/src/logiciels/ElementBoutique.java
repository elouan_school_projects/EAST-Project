package logiciels;

public class ElementBoutique extends ElementStand {

	// CONSTRUCTEUR
	public ElementBoutique(String nom, float prixElementBoutique, int quantite) {
		super(nom, prixElementBoutique, quantite);
	}

	// METHODE DE CALCUL DU COUT
	public float calculCout() {
		return this.getQuantite() * this.getPrix();
	}

}
