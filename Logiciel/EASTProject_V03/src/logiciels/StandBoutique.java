package logiciels;

import java.util.ArrayList;

public class StandBoutique extends Stand {
	private ArrayList<ElementBoutique> listeElementsBoutique;

	public StandBoutique(String n, String taille, float hauteur, float longueur, float largeur) {
		super(n, taille, hauteur, longueur, largeur);
		listeElementsBoutique = new ArrayList<ElementBoutique>();
	}

	// AJOUTER UN ELEMENT DE BOUTIQUE AU STAND
	public void ajoutElementNourriture(ElementBoutique b) {
		listeElementsBoutique.add(b);
	}

	// MODIFIER UN ELEMENT DE BOUTIQUE DU STAND
	public void editerElementNourriture(ElementBoutique b) {
		int i = 0;
		boolean trouve = false;
		while ((i <= listeElementsBoutique.size()) && (trouve == false)) {
			if (listeElementsBoutique.get(i).equals(b)) {
				trouve = true;
			} else {
				i++;
			}
		}
		if (trouve) {
			listeElementsBoutique.set(i, b);
		} else {
			System.out.println("Erreur : l'article " + b + "n'existe pas dans la liste");
		}
	}

	// SUPPRIMMER UN ELEMENT DE BOUTIQUE DU STAND
	public void supprimmerElementNourriture(ElementBoutique b) {
		if (listeElementsBoutique.contains(b)) {
			listeElementsBoutique.remove(b);
		} else {
			System.out.println("Erreur : l'article " + b + "n'existe pas dans la liste");
		}
	}

	// METHODE DE CALCUL DU COUT
	@Override
	public float calculCout() {
		float coutTotal = 0;

		if (listeElementsBoutique.isEmpty()) {
			System.out.println("Erreur : pas d'Article assign�e � ce stand");
		} else {
			for (int i = 0; i < listeElementsBoutique.size(); i++) {
				coutTotal += listeElementsBoutique.get(i).calculCout();
			}
		}
		cout += coutTotal;
		return cout;
	}

	// METHODES AFFICHAGE
	public String listerElementsBoutique() {
		String s = new String("\n - contenu du stand :\n");
		if (listeElementsBoutique.size() == 0) {
			s += "	> vide";
		} else {
			for (int i = 0; i < listeElementsBoutique.size(); i++) {
				s += ("	> " + listeElementsBoutique.get(i) + "\n");
			}
		}
		return s;
	}

	@Override
	public String toString() {
		return super.toString() + this.listerElementsBoutique();
	}

	// METHODE OBTENTION DU NOM DE LA CLASSE
	@Override
	public String getNomClasse() {
		return "Stand Boutique";
	}

}
