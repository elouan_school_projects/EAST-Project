
package interfaces;

import java.util.ArrayList;

import javafx.scene.Parent;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import logiciels.ProjetFestival;
import logiciels.StandBoisson;
import logiciels.StandBoutique;
import logiciels.StandConversion;
import logiciels.StandNourriture;
import logiciels.StandPreventionSecours;
import logiciels.StandSanitaire;
import logiciels.StandScene;

public class FenetreHistorique {
	private ArrayList<StandBoisson> listeStandBoisson;
	private ArrayList<StandBoutique> listeStandBoutique;
	private ArrayList<StandConversion> listeStandConversion;
	private ArrayList<StandNourriture> listeStandNourriture;
	private ArrayList<StandPreventionSecours> listestandPreventionSecours;
	private ArrayList<StandSanitaire> listeStandSanitaire;
	private ArrayList<StandScene> listeStandScene;
	private ProjetFestival projet;
	private TreeView<String> treeView;

	public FenetreHistorique(ProjetFestival p) {

		projet = p;
		listeStandBoisson = new ArrayList<StandBoisson>();
		listeStandBoutique = new ArrayList<StandBoutique>();
		listeStandConversion = new ArrayList<StandConversion>();
		listeStandNourriture = new ArrayList<StandNourriture>();
		listestandPreventionSecours = new ArrayList<StandPreventionSecours>();
		listeStandSanitaire = new ArrayList<StandSanitaire>();
		listeStandScene = new ArrayList<StandScene>();

		// TRI DES STANDS DANS ARRAYLIST
		if (projet.getCarte().getStand().isEmpty()) {
			System.out.println("pas de stand dans carte");
		} else {
			for (int i = 0; i < projet.getCarte().getStand().size(); i++) {

				if (projet.getCarte().getStand().get(i).getNomClasse().equals("Stand Boisson")) {
					listeStandBoisson.add((StandBoisson) projet.getCarte().getStand().get(i));
				} else if (projet.getCarte().getStand().get(i).getNomClasse() == "Stand Boutique") {
					listeStandBoutique.add((StandBoutique) projet.getCarte().getStand().get(i));
				} else if (projet.getCarte().getStand().get(i).getNomClasse() == "Stand Conversion") {
					listeStandConversion.add((StandConversion) projet.getCarte().getStand().get(i));
				} else if (projet.getCarte().getStand().get(i).getNomClasse() == "Stand Nourriture") {
					listeStandNourriture.add((StandNourriture) projet.getCarte().getStand().get(i));
				} else if (projet.getCarte().getStand().get(i).getNomClasse() == "Stand Prévention Secours") {
					listestandPreventionSecours.add((StandPreventionSecours) projet.getCarte().getStand().get(i));
				} else if (projet.getCarte().getStand().get(i).getNomClasse() == "Stand Sanitaire") {
					listeStandSanitaire.add((StandSanitaire) projet.getCarte().getStand().get(i));
				} else if (projet.getCarte().getStand().get(i).getNomClasse() == "Stand Scene") {
					listeStandScene.add((StandScene) projet.getCarte().getStand().get(i));
				}

			}
		}
	}

	public Parent affichage() {
		// DEBUT CREATION DE LA BASE DU TREEVIEW
		treeView = new TreeView<String>();
		
		TreeItem<String> rootItem = new TreeItem<String>("Liste :");
		TreeItem<String> standItem = new TreeItem<String>("Stands");
		final TreeItem<String> artisteItem = new TreeItem<String>("Artiste");
		
		treeView.setPrefHeight(750);
		// FIN CREATION DE LA BASE DU TREEVIEW

		// DEBUT CREATION DES TREE ITEM EN FONCTION DES STAND INSTANCIES
		ArrayList<TreeItem<String>> artistet = new ArrayList<TreeItem<String>>();
		for (int i = 0; i < projet.getCarte().getArtiste().size(); i++) {
			artistet.add(new TreeItem<String>(projet.getCarte().getArtiste().get(i).toString()));
		}

		for (int i = 0; i < artistet.size(); i++) {
			artisteItem.getChildren().add(artistet.get(i));
		}

		if (!this.listeStandBoisson.isEmpty()) {
			ArrayList<TreeItem<String>> standboissont = new ArrayList<TreeItem<String>>();
			for (int i = 0; i < this.listeStandBoisson.size(); i++) {
				standboissont.add(new TreeItem<String>(this.listeStandBoisson.get(i).toString()));
			}

			final TreeItem<String> standBoissonItem = new TreeItem<String>("Stand Boisson");
			for (int i = 0; i < standboissont.size(); i++) {
				standBoissonItem.getChildren().add(standboissont.get(i));
			}

			standItem.getChildren().add(standBoissonItem);

		}
		if (!this.listeStandBoutique.isEmpty()) {

			ArrayList<TreeItem<String>> standboutiquet = new ArrayList<TreeItem<String>>();
			for (int i = 0; i < this.listeStandBoutique.size(); i++) {
				standboutiquet.add(new TreeItem<String>(this.listeStandBoutique.get(i).toString()));
			}
			final TreeItem<String> standBoutiqueItem = new TreeItem<String>("Stand Boutique");
			for (int i = 0; i < standboutiquet.size(); i++) {
				standBoutiqueItem.getChildren().add(standboutiquet.get(i));
			}
			standItem.getChildren().add(standBoutiqueItem);
		}

		if (!this.listeStandConversion.isEmpty()) {

			ArrayList<TreeItem<String>> standconversiont = new ArrayList<TreeItem<String>>();
			for (int i = 0; i < this.listeStandConversion.size(); i++) {
				standconversiont.add(new TreeItem<String>(this.listeStandConversion.get(i).toString()));
			}
			final TreeItem<String> standConversionItem = new TreeItem<String>("Stand Conversion");
			for (int i = 0; i < standconversiont.size(); i++) {
				standConversionItem.getChildren().add(standconversiont.get(i));
			}
			standItem.getChildren().add(standConversionItem);
		}

		if (!this.listeStandNourriture.isEmpty()) {

			ArrayList<TreeItem<String>> standnourrituret = new ArrayList<TreeItem<String>>();
			for (int i = 0; i < this.listeStandNourriture.size(); i++) {
				standnourrituret.add(new TreeItem<String>(this.listeStandNourriture.get(i).toString()));
			}
			final TreeItem<String> standNourritureItem = new TreeItem<String>("Stand Nourriture");
			for (int i = 0; i < standnourrituret.size(); i++) {
				standNourritureItem.getChildren().add(standnourrituret.get(i));
			}
			standItem.getChildren().add(standNourritureItem);
		}

		if (!this.listestandPreventionSecours.isEmpty()) {

			ArrayList<TreeItem<String>> standprevt = new ArrayList<TreeItem<String>>();
			for (int i = 0; i < this.listestandPreventionSecours.size(); i++) {
				standprevt.add(new TreeItem<String>(this.listestandPreventionSecours.get(i).toString()));
			}
			final TreeItem<String> standPrevItem = new TreeItem<String>("Stand Prevention Secours");
			for (int i = 0; i < standprevt.size(); i++) {
				standPrevItem.getChildren().add(standprevt.get(i));
			}
			standItem.getChildren().add(standPrevItem);
		}
		if (!this.listeStandSanitaire.isEmpty()) {

			ArrayList<TreeItem<String>> standsanitairet = new ArrayList<TreeItem<String>>();
			for (int i = 0; i < this.listeStandSanitaire.size(); i++) {
				standsanitairet.add(new TreeItem<String>(this.listeStandSanitaire.get(i).toString()));
			}
			final TreeItem<String> standSanitaireItem = new TreeItem<String>("Stand Sanitaire");
			for (int i = 0; i < standsanitairet.size(); i++) {
				standSanitaireItem.getChildren().add(standsanitairet.get(i));
			}
			standItem.getChildren().add(standSanitaireItem);
		}
		if (!this.listeStandScene.isEmpty()) {

			ArrayList<TreeItem<String>> standscenet = new ArrayList<TreeItem<String>>();
			for (int i = 0; i < this.listeStandScene.size(); i++) {
				standscenet.add(new TreeItem<String>(this.listeStandScene.get(i).toString()));
			}
			final TreeItem<String> standSceneItem = new TreeItem<String>("Stand Scene");
			for (int i = 0; i < standscenet.size(); i++) {
				standSceneItem.getChildren().add(standscenet.get(i));
			}

			standItem.getChildren().add(standSceneItem);

		}

		rootItem.setExpanded(true);
		rootItem.getChildren().addAll(standItem, artisteItem);
		// FIN CREATION DES TREE ITEM EN FONCTION DES STAND INSTANCIES

		// DEBUT DES ELEMENTS DE BASE DE FONCTIONNEMENT
		treeView.setRoot(rootItem);
		return treeView;
	}

	public TreeView<String> getTreeView() {
		return treeView;
	}

	public void setTreeView(TreeView<String> treeView) {
		this.treeView = treeView;
	}

}
