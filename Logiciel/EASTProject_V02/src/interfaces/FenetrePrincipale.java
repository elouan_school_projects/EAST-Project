package interfaces;

import java.util.Date;

import com.sun.javafx.cursor.CursorFrame;
import com.sun.javafx.cursor.CursorType;
import com.sun.javafx.cursor.ImageCursorFrame;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Modality;
import javafx.stage.Stage;
import logiciels.ProjetFestival;

public class FenetrePrincipale extends Stage {
	private ProjetFestival projet;

	private BorderPane panePrincipal;
	private VBox partieGauche;
	private VBox partieCentrale;

	private MenuBar menuPrincipal;
	private Menu menuFichier;
	private MenuItem nouveau;
	private MenuItem ouvrir;
	private MenuItem enregistrer;
	private MenuItem enregistrerSous;
	private MenuItem quitter;

	private FenetreHistorique historique;

	private TabPane onglets;
	private Tab ongletStands;
	private Tab ongletArtistes;
	private Tab ongletPlanning;
	private Tab ongletComptabilitee;
	private Tab ongletCarte;

	private HBox menuFoncArtistes;
	private HBox menuFoncStands;
	private Button ajouterStands;
	private Button editerStands;
	private Button supprimerStands;
	private Button ajouterArtistes;
	private Button editerArtistes;
	private Button supprimerArtistes;

	private StackPane affichageCarte;
	private Rectangle fondCarte;

	private FenetreComptabilite compta = new FenetreComptabilite(0);

	private float largeurEcran;
	private float hauteurEcran;

	public FenetrePrincipale() {
		this.setTitle("EAST");
		this.setX(0);
		this.setY(0);
		largeurEcran = 1300;
		hauteurEcran = 800;
		this.setResizable(false);

		// ------INITIALISATION DES DIFFERENTS ELEMENTS------
		// Groupe principal
		panePrincipal = new BorderPane();
		partieCentrale = new VBox();
		partieGauche = new VBox();
		// Menu
		menuPrincipal = new MenuBar();
		menuFichier = new Menu("Fichier");
		nouveau = new MenuItem("Nouveau Projet");
		ouvrir = new MenuItem("Ouvrir Projet");
		enregistrer = new MenuItem("Enregistrer");
		enregistrerSous = new MenuItem("Enregistrer Sous");
		quitter = new MenuItem("Quitter");
		this.initMenuPrincipal();
		// Historique
		historique = new FenetreHistorique();
		// Menu Fonctionnalit�es
		menuFoncArtistes = new HBox();
		menuFoncStands = new HBox();
		ajouterStands = new Button("Ajouter Stand");
		editerStands = new Button("Editer Stand");
		supprimerStands = new Button("Supprimer Stand");
		ajouterArtistes = new Button("Ajouter Artiste");
		editerArtistes = new Button("Editer Artiste");
		supprimerArtistes = new Button("Supprimer Artiste");
		this.initMenuFonc();
		// Carte interactive
		fondCarte = new Rectangle();
		this.initCarte();
		// Onglets
		onglets = new TabPane();
		ongletStands = new Tab();
		ongletArtistes = new Tab();
		ongletPlanning = new Tab();
		ongletComptabilitee = new Tab();
		ongletCarte = ongletStands;
		this.initOnglets();
		// Boutons
		this.initButton();

		// ------INITIALISATION DE LA SCENE------
		Scene scene = new Scene(creerContenu(), this.getWidth(), this.getHeight());
		this.setHeight(hauteurEcran);
		this.setWidth(largeurEcran);
		this.setScene(scene);

		// ------INITIALISATION DES PARAMETRES DES ELEMENTS------
		partieGauche.setPrefWidth(largeurEcran / 10);
		partieCentrale.setPrefWidth(largeurEcran * (9.0 / 10.0));
	}

	private void initMenuPrincipal() {
		// Cr�ation du menu principal
		menuPrincipal.getMenus().add(menuFichier);

		// Bouton "Nouveau Projet" du menu principal
		nouveau.setOnAction(e -> {
			System.out.println("Nouveau Projet");
		});
		// Bouton "Ouvrir Projet" du menu principal
		ouvrir.setOnAction(e -> {
			System.out.println("Ouvrir Projet");
		});
		// Bouton "Enregistrer" du menu principal
		enregistrer.setOnAction(e -> {
			System.out.println("Enregistrer");
		});
		// Bouton "Enregistrer Sous" du menu principal
		enregistrerSous.setOnAction(e -> {
			System.out.println("Enregistrer Sous");
		});
		// Bouton "Quitter" du menu principal
		quitter.setOnAction(e -> {
			System.exit(0);
		});

		// Ajout des boutons au menu principal
		menuFichier.getItems().addAll(nouveau, ouvrir, new SeparatorMenuItem(), enregistrer, enregistrerSous,
				new SeparatorMenuItem(), quitter);
	}

	private void initOnglets() {
		// Cr�ation de l'onglet de gestion des Stands
		VBox boxOngletStands = new VBox();
		boxOngletStands.getChildren().addAll(menuFoncStands, affichageCarte);
		ongletStands.setText("Gestion des Stands");
		ongletStands.setContent(boxOngletStands);
		ongletStands.setClosable(false);

		// Cr�ation de l'onglet de gestion des Artistes
		VBox boxOngletArtistes = new VBox();
		boxOngletArtistes.getChildren().addAll(menuFoncArtistes);
		ongletArtistes.setText("Gestion des Artistes");
		ongletArtistes.setContent(boxOngletArtistes);
		ongletArtistes.setClosable(false);

		// Cr�ation de l'onglet de gestion du Planning
		ongletPlanning.setText("Gestion du Planning");
		ongletPlanning.setContent(new Rectangle(200, 200, Color.YELLOW));
		ongletPlanning.setClosable(false);

		// Cr�ation de l'onglet de gestion de la comptabilit�e

		ongletComptabilitee.setText("Gestion de la Comptabilit�");
		ongletComptabilitee.setContent(compta.affichage());
		ongletComptabilitee.setClosable(false);

		// Ajout des onglets dans la liste des onglets
		onglets.getTabs().addAll(ongletStands, ongletArtistes, ongletPlanning, ongletComptabilitee);

		// Gestion du changment d'onglet pour afficher un m�me �lement dans un onglet

		onglets.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
			@Override
			public void changed(ObservableValue<? extends Tab> observable, Tab ancientOnglet, Tab nouvelonglet) {
				if ((nouvelonglet == ongletStands) && (ongletCarte == ongletArtistes)) {
					boxOngletArtistes.getChildren().remove(affichageCarte);
					boxOngletStands.getChildren().add(affichageCarte);
					ongletCarte = ongletStands;
					
				} else if ((nouvelonglet == ongletArtistes) && (ongletCarte == ongletStands)) {
					boxOngletStands.getChildren().remove(affichageCarte);
					boxOngletArtistes.getChildren().add(affichageCarte);
					ongletCarte = ongletArtistes;
				}
			}
		});
		

	}

	private void initMenuFonc() {
		// Creation Menu Fonctionnalit�es de la gestion des stands
		menuFoncStands.getChildren().addAll(ajouterStands, editerStands, supprimerStands);

		// Creation Menu Fonctionnalit�es de la gestion des artistes
		menuFoncArtistes.getChildren().addAll(ajouterArtistes, editerArtistes, supprimerArtistes);
	}

	private void initButton() {
		ajouterStands.setOnAction(e1 -> {
			editerStands.setDisable(true);
			supprimerStands.setDisable(true);
			CreationStand fen = new CreationStand();
			fen.initModality(Modality.WINDOW_MODAL);
			fen.initOwner(this.getScene().getWindow());
			fen.show();

			// mage image = new Image("mycursor.png");
			// Rectangle test = new Rectangle();
			// this.getScene().setCursor(new ImageCursor(test,122,122));

			// ajout stand � la carte
			// projet.getCarte().ajoutStand(s);

			/*
			 * this.getScene().setCursor(Cursor.HAND);
			 * ongletStands.getContent().setOnMouseClicked(e2 -> { MouseButton button =
			 * e2.getButton(); if (button == MouseButton.SECONDARY) {
			 * editerStands.setDisable(false); supprimerStands.setDisable(false);
			 * this.getScene().setCursor(Cursor.DEFAULT); } });
			 * ongletStands.getContent().setOnMouseExited(e3 -> {
			 * editerStands.setDisable(false); supprimerStands.setDisable(false);
			 * this.getScene().setCursor(Cursor.DEFAULT); });
			 * affichageCarte.setOnMouseClicked(e2 -> { MouseButton button = e2.getButton();
			 * if ((this.getScene().getCursor() == Cursor.HAND) && (button !=
			 * MouseButton.SECONDARY)) { CreationStand fen = new CreationStand();
			 * fen.initModality(Modality.WINDOW_MODAL);
			 * fen.initOwner(this.getScene().getWindow()); fen.show();
			 * editerStands.setDisable(false); supprimerStands.setDisable(false);
			 * this.getScene().setCursor(Cursor.DEFAULT); } });
			 */
		});

	}

	private void initCarte() {
		affichageCarte = new StackPane();
		fondCarte = new Rectangle(largeurEcran, hauteurEcran, Color.RED);
		affichageCarte.getChildren().add(fondCarte);
	}

	public void initProjet() {
		this.setTitle(projet.getNom());
		compta.setBudget(projet.getBudget());
	}

	public void setProjetFestival(ProjetFestival projet) {
		this.projet = projet;
	}
	

	public Parent creerContenu() {
		Group root = new Group();
		// ------PARTIE GAUCHE------
		partieGauche.getChildren().addAll(menuPrincipal, historique.affichage());
		panePrincipal.setLeft(partieGauche);

		// ------PARTIE CENTRALE------
		partieCentrale.getChildren().addAll(onglets);
		panePrincipal.setCenter(partieCentrale);

		root.getChildren().add(panePrincipal);
		return root;
	}

}