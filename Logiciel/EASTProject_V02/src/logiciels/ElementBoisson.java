package logiciels;

public class ElementBoisson extends ElementStand {
	

	// CONSTRUCTEUR
	public ElementBoisson(String nom, float prixElementBoisson, int quantite) {
		super(nom,prixElementBoisson,quantite);
	}

	
	

	// METHODE DE CALCUL DU COUT
	public float calculCout() {
		return this.getQuantite()*this.getPrix();
	}
}
