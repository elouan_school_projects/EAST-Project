package logiciels;

public class ElementScene extends ElementStand {


	// CONSTRUCTEUR
	public ElementScene(String nom, float prixElementScene, int quantite) {
		super(nom,prixElementScene,quantite);
	}


	// METHODE DE CALCUL DU COUT
	public float calculCout() {
		return this.getQuantite()*this.getPrix();
	}

}
