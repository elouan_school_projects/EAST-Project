package logiciels;

import java.util.ArrayList;

public class StandSanitaire extends Stand {
	private ArrayList<ElementSanitaire> listeElementsSanitaires;

	public StandSanitaire(String n, String taille, float hauteur, float longueur, float largeur) {
		super(n, taille, hauteur, longueur, largeur);
		this.listeElementsSanitaires = new ArrayList<ElementSanitaire>();
	}

	// AJOUTER UN ELEMENT DE SANNITAIRE AU STAND
	public void ajoutElementSanitaire(ElementSanitaire s) {
		listeElementsSanitaires.add(s);
	}

	// MODIFIER UN ELEMENT DE SANNITAIRE DU STAND
	public void editerElementSanitaire(ElementSanitaire s) {
		int i = 0;
		boolean trouve = false;
		while ((i <= listeElementsSanitaires.size()) && (trouve == false)) {
			if (listeElementsSanitaires.get(i).equals(s)) {
				trouve = true;
			} else {
				i++;
			}
		}
		if (trouve) {
			listeElementsSanitaires.set(i, s);
		} else {
			System.out.println("Erreur : la caravane " + s + "n'existe pas dans la liste");
		}
	}

	// SUPPRIMMER UN ELEMENT DE SANNITAIRE DU STAND
	public void supprimmerElementSanitaire(ElementSanitaire s) {
		if (listeElementsSanitaires.contains(s)) {
			listeElementsSanitaires.remove(s);
		} else {
			System.out.println("Erreur : la caravane " + s + "n'existe pas dans la liste");
		}
	}

	// METHODE DE CALCUL DU COUT
	@Override
	public float calculCout() {
		float coutTotal = 0;

		if (listeElementsSanitaires.isEmpty()) {
			System.out.println("Erreur : pas de ElementSanitaire assign� � ce stand");
		} else {
			for (int i = 0; i < listeElementsSanitaires.size(); i++) {
				coutTotal += listeElementsSanitaires.get(i).calculCout();
			}
		}
		cout += coutTotal;
		return cout;
	}

	// METHODES AFFICHAGE
	public String listerElementsSanitaires() {
		String s = new String("\n - contenu du stand :\n");
		if (listeElementsSanitaires.size() == 0) {
			s += "	> vide";
		} else {
			for (int i = 0; i < listeElementsSanitaires.size(); i++) {
				s += ("	> " + listeElementsSanitaires.get(i) + "\n");
			}
		}
		return s;
	}

	@Override
	public String toString() {
		return super.toString() + this.listerElementsSanitaires();
	}

	// METHODE OBTENTION NOM CLASSE
	@Override
	public String getNomClasse() {
		return "Stand Sanitaire";
	}

}
