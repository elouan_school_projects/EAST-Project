package interfaces;

import javafx.beans.binding.BooleanBinding;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import logiciels.ElementBoutique;
import logiciels.ElementPreventionSecours;
import logiciels.ElementBoisson;
import logiciels.ElementSanitaire;
import logiciels.ElementScene;
import logiciels.ElementStand;
import logiciels.Stand;
import logiciels.StandBoisson;
import logiciels.StandBoutique;
import logiciels.StandConversion;
import logiciels.StandNourriture;
import logiciels.StandPreventionSecours;
import logiciels.StandSanitaire;
import logiciels.StandScene;
import logiciels.ElementNourriture;

public class ModifierStand extends Stage {

	private static Stand stand;

	private VBox listesTop = new VBox();
	private VBox texteGauche = new VBox();
	private VBox textfieldDroite = new VBox();
	private HBox creerRepas = new HBox();
	private HBox supprRepas = new HBox();
	private HBox boutons_bot = new HBox();
	private HBox nomStand = new HBox();
	private GridPane saisie = new GridPane();
	private Label hauteur = new Label("Hauteur (m) :");
	private Label longueur = new Label("Longueur (m) :");
	private Label largeur = new Label("Largeur (m) :");
	private Label repasDispo = new Label("Repas Disponibles :");
	private Label repasAjouter = new Label("Repas :");
	private Label nomS = new Label("Nom :");
	private TextField hauteurS = new TextField();
	private TextField longueurS = new TextField();
	private TextField largeurS = new TextField();
	private TextField nom = new TextField();
	private TextField prix = new TextField();
	private TextField quantite = new TextField();
	private Button suppr = new Button("Supprimer");
	private Button ajout = new Button("Ajouter");
	private Button placer = new Button("Valider");
	private Button annuler = new Button("Annuler");
	private TextField nomText = new TextField();
	private TextField nbrobinet = new TextField();
	private TextField nbtoilette = new TextField();
	private Label typeStand = new Label();
	private ComboBox<String> tailleStand;
	private ComboBox<ElementStand> listeElementNourriture;
	private ComboBox<ElementStand> listeElementBoisson;
	private ComboBox<ElementStand> listeElementBoutique;
	private ComboBox<ElementStand> listeElementSanitaire;
	private ComboBox<ElementStand> listeElementScene;
	private ComboBox<ElementStand> listeElementPrevention;
	private ObservableList<String> stands;
	private ObservableList<ElementStand> repas;
	private ObservableList<ElementStand> Caravanes;
	private ObservableList<ElementStand> ArticlesPrev;
	private ObservableList<ElementStand> ArticlesBoutiques;
	private ObservableList<ElementStand> Boissons;
	private ObservableList<ElementStand> Materiel;

	public ModifierStand(Stand s) {
		this.setTitle("Editer Stand");
		this.setResizable(false);

		stand = s;
		Scene laScene = new Scene(affichage(), 740, 350);
		this.setScene(laScene);

	}

	Parent affichage() {

		VBox root = new VBox();

		// TYPE STAND
		typeStand = new Label("Edition " + stand.getNomClasse());
		typeStand.setAlignment(Pos.BASELINE_CENTER);

		// NOM
		nomStand.getChildren().addAll(nomS, nomText);
		nomStand.setSpacing(100);
		nomText.setMaxWidth(565);
		HBox.setHgrow(nomText, Priority.ALWAYS);

		listesTop.getChildren().add(0, nomStand);

		// LISTE_DEROULANTE
		stands = FXCollections.observableArrayList("Stand nourriture", "Stand sanitaire", "Stand scene",
				"Stand prevention", "Stand boutique", "Stand conversion", "Stand boisson");

		ObservableList<String> taille = FXCollections.observableArrayList("Stand Petit", "Stand Moyen", "Stand Grand");

		tailleStand = new ComboBox<String>(taille);

		tailleStand.setPromptText("Selectionnez une taille de stand");

		listesTop.getChildren().add(tailleStand);
		listesTop.setSpacing(7);

		tailleStand.setMaxWidth(700);

		VBox.setVgrow(tailleStand, Priority.ALWAYS);

		listesTop.setMinWidth(tailleStand.getWidth());

		// TEXT_GAUCHE

		texteGauche.getChildren().addAll(hauteur, longueur, largeur, repasDispo, repasAjouter);
		texteGauche.setSpacing(15);
		saisie.add(texteGauche, 0, 0);
		repas = FXCollections.observableArrayList(

		);

		// TEXTFIELDS_DROITE
		// 0

		listeElementNourriture = new ComboBox<ElementStand>(repas);
		// 1
		Boissons = FXCollections.observableArrayList(

		);
		listeElementBoisson = new ComboBox<ElementStand>(Boissons);
		// 2
		ArticlesPrev = FXCollections.observableArrayList(

		);
		listeElementPrevention = new ComboBox<ElementStand>(ArticlesPrev);
		// 3
		Caravanes = FXCollections.observableArrayList(

		);
		listeElementSanitaire = new ComboBox<ElementStand>(Caravanes);
		// 4
		ArticlesBoutiques = FXCollections.observableArrayList(

		);
		listeElementBoutique = new ComboBox<ElementStand>(ArticlesBoutiques);
		// 5
		Materiel = FXCollections.observableArrayList(

		);
		listeElementScene = new ComboBox<ElementStand>(Materiel);

		listeElementNourriture.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(listeElementNourriture, Priority.ALWAYS);
		suppr.setMinWidth(50);
		suppr.setMaxWidth(85);
		HBox.setHgrow(suppr, Priority.ALWAYS);
		supprRepas.getChildren().addAll(listeElementNourriture, suppr);
		supprRepas.setSpacing(10);
		creerRepas.getChildren().addAll(nom, prix, quantite, ajout);

		nom.setPromptText("Nom");
		prix.setPromptText("Prix");
		quantite.setPromptText("Quantite");

		nom.setMaxWidth(Double.MAX_VALUE);
		prix.setMaxWidth(Double.MAX_VALUE);
		quantite.setMaxWidth(Double.MAX_VALUE);

		HBox.setHgrow(nom, Priority.ALWAYS);
		HBox.setHgrow(prix, Priority.ALWAYS);
		HBox.setHgrow(quantite, Priority.ALWAYS);
		HBox.setHgrow(nbrobinet, Priority.ALWAYS);
		HBox.setHgrow(nbtoilette, Priority.ALWAYS);

		creerRepas.setSpacing(10);

		ajout.setMinWidth(85);
		ajout.setMaxWidth(85);

		textfieldDroite.getChildren().addAll(hauteurS, longueurS, largeurS, supprRepas, creerRepas);

		textfieldDroite.setSpacing(5);
		saisie.add(textfieldDroite, 1, 0);
		textfieldDroite.setPadding(new Insets(0, 0, 0, 30));
		// saisie.setGridLinesVisible(true);

		// BOUTONS BOTTOM

		boutons_bot.getChildren().addAll(placer, annuler);
		boutons_bot.setAlignment(Pos.BASELINE_CENTER);
		boutons_bot.setSpacing(100);
		placer.setMinWidth(200);
		annuler.setMinWidth(200);

		// GESTION DE LA SAISIE
		EntierRelatif(hauteurS);
		EntierRelatif(longueurS);
		EntierRelatif(largeurS);
		EntierRelatif(prix);
		Entier(quantite);
		QuelStand();

		root.getChildren().addAll(this.typeStand, this.listesTop, saisie, boutons_bot);
		root.setSpacing(20);
		root.setPadding(new Insets(20));
		// GRISER LE BOUTON VALIDER EN FONCTION DES TEXTFIELDS
		BooleanBinding bb1 = new BooleanBinding() {
			{
				super.bind(hauteurS.textProperty(), largeurS.textProperty(), longueurS.textProperty(),
						nomText.textProperty(), tailleStand.getSelectionModel().selectedItemProperty());
			}

			@Override
			protected boolean computeValue() {
				return (hauteurS.getText().isEmpty() || largeurS.getText().isEmpty() || longueurS.getText().isEmpty()
						|| nomText.getText().isEmpty() || tailleStand.getSelectionModel().getSelectedItem() == null);

			}

		};
		BooleanBinding bb2 = new BooleanBinding() {
			{
				super.bind(prix.textProperty(), nom.textProperty(), quantite.textProperty()

				);
			}

			@Override
			protected boolean computeValue() {

				return (prix.getText().isEmpty() || nom.getText().isEmpty() || quantite.getText().isEmpty());

			}

		};

		ajout.disableProperty().bind(bb2);
		placer.disableProperty().bind(bb1);

		// FERMER LA FENETRE
		annuler.setOnAction(e -> {

			this.close();
		});

		// MODIFICATION DE LA FENETRE ET DES INTERRACTIONS EN FONCTION DU TYPE DE STAND
		// SELECTIONNE

		return root;
	}

	// EMPECHER LA SAISIE DE CERTAINS CARACTERES
	public void Entier(TextField tf) {
		tf.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("[\\d]*")) {
					tf.setText(newValue.replaceAll("[^\\d]", ""));
				}
			}
		});
	}

	public void EntierRelatif(TextField tf) {
		tf.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("[\\d.]*")) {
					tf.setText(newValue.replaceAll("[^\\d.]", ""));
				}
			}
		});
	}

	// AJOUTER DANS LA LISTE DES ELEMENTS DU STAND
	public void ajouterlisteelement(ObservableList<ElementStand> l, ComboBox<ElementStand> cb, int id) {

		int i = 0;
		int j = 0;
		boolean trouve = false;
		@SuppressWarnings("unused")
		boolean trouve2 = false;
		if (nom.getText().length() == 0 || prix.getText().length() == 0 || quantite.getText().length() == 0) {
			System.out.println("Champs non saisies");
		} else {
			while (i < l.size() && !trouve) {
				if (l.get(i).getNom().equals(nom.getText()) && l.get(i).getPrix() == Float.parseFloat(prix.getText())) {
					l.get(i).setQuantite(l.get(i).getQuantite() + Integer.parseInt(quantite.getText()));
					ElementStand n = l.get(i);
					l.remove(i);
					l.add(n);
					while (j < l.size() && !trouve) {
						if (l.get(j) == n) {
							cb.setValue(l.get(j));
							trouve2 = true;
						}
						j++;
					}
					trouve = true;
				}
				i++;
			}
			if (!trouve) {
				switch (id) {
				case 0:
					l.add(new ElementNourriture(nom.getText(), Float.parseFloat(prix.getText()),
							Integer.parseInt(quantite.getText())));
					cb.setValue(l.get(l.size() - 1));
					break;

				case 1:
					l.add(new ElementBoisson(nom.getText(), Float.parseFloat(prix.getText()),
							Integer.parseInt(quantite.getText())));
					cb.setValue(l.get(l.size() - 1));
					break;

				case 2:
					l.add(new ElementBoutique(nom.getText(), Float.parseFloat(prix.getText()),
							Integer.parseInt(quantite.getText())));
					cb.setValue(l.get(l.size() - 1));
					break;

				case 3:
					l.add(new ElementSanitaire(nom.getText(), Float.parseFloat(prix.getText()),
							Integer.parseInt(quantite.getText()), Integer.parseInt(nbrobinet.getText()),
							Integer.parseInt(nbtoilette.getText())));
					cb.setValue(l.get(l.size() - 1));
					break;

				case 4:
					l.add(new ElementScene(nom.getText(), Float.parseFloat(prix.getText()),
							Integer.parseInt(quantite.getText())));
					cb.setValue(l.get(l.size() - 1));
					break;

				case 5:
					l.add(new ElementPreventionSecours(nom.getText(), Float.parseFloat(prix.getText()),
							Integer.parseInt(quantite.getText())));
					cb.setValue(l.get(l.size() - 1));
					break;
				}
			}
		}

	}

	// SUPPRIMER DANS LA LISTE DES ELEMENTS DU STAND
	public void supprimerlistelement(ObservableList<ElementStand> l, ComboBox<ElementStand> cb) {
		if (l.size() != 0) {
			l.remove(cb.getSelectionModel().getSelectedItem());
			if (l.size() != 0) {
				cb.setValue(l.get(l.size() - 1));
			}

		} else {
			System.out.println("liste vide");
		}
	}

	public static Stand getStand() {
		return ModifierStand.stand;
	}

	// AJOUTER LE STAND A LA CARTE
	public void AjoutStand(ObservableList<ElementStand> listeElement, int id) {

		switch (id) {
		case 0:
			StandNourriture sn = new StandNourriture(nomText.getText(),
					tailleStand.getSelectionModel().getSelectedItem(), Float.parseFloat(hauteurS.getText()),
					Float.parseFloat(this.longueurS.getText()), Float.parseFloat(largeurS.getText()));
			for (int i = 0; i < listeElement.size(); i++) {
				sn.ajouElementNourriture((ElementNourriture) listeElement.get(i));
				sn.setAffichage(stand.getAffichage());
				sn.setNumero(ModifierStand.stand.getNumero());
			}
			ModifierStand.stand = sn;
			this.close();
			break;

		case 1:
			StandBoisson sb = new StandBoisson(nomText.getText(), tailleStand.getSelectionModel().getSelectedItem(),
					Float.parseFloat(hauteurS.getText()), Float.parseFloat(this.longueurS.getText()),
					Float.parseFloat(largeurS.getText()));
			for (int i = 0; i < listeElement.size(); i++) {
				sb.ajoutElementBoisson((ElementBoisson) listeElement.get(i));
				sb.setAffichage(stand.getAffichage());
				sb.setNumero(ModifierStand.stand.getNumero());
			}
			ModifierStand.stand = sb;
			this.close();
			break;

		case 2:
			StandBoutique sbou = new StandBoutique(nomText.getText(), tailleStand.getSelectionModel().getSelectedItem(),
					Float.parseFloat(hauteurS.getText()), Float.parseFloat(this.longueurS.getText()),
					Float.parseFloat(largeurS.getText()));
			for (int i = 0; i < listeElement.size(); i++) {
				sbou.ajoutElementNourriture((ElementBoutique) listeElement.get(i));
				sbou.setAffichage(stand.getAffichage());
				sbou.setNumero(ModifierStand.stand.getNumero());
			}
			ModifierStand.stand = sbou;
			this.close();
			break;

		case 3:
			StandSanitaire ssan = new StandSanitaire(nomText.getText(),
					tailleStand.getSelectionModel().getSelectedItem(), Float.parseFloat(hauteurS.getText()),
					Float.parseFloat(this.longueurS.getText()), Float.parseFloat(largeurS.getText()));
			for (int i = 0; i < listeElement.size(); i++) {
				ssan.ajoutElementSanitaire((ElementSanitaire) listeElement.get(i));
				ssan.setAffichage(stand.getAffichage());
				ssan.setNumero(ModifierStand.stand.getNumero());
			}
			ModifierStand.stand = ssan;
			this.close();
			break;

		case 4:
			StandScene ssce = new StandScene(nomText.getText(), tailleStand.getSelectionModel().getSelectedItem(),
					Float.parseFloat(hauteurS.getText()), Float.parseFloat(this.longueurS.getText()),
					Float.parseFloat(largeurS.getText()));
			for (int i = 0; i < listeElement.size(); i++) {
				ssce.ajoutElementScene((ElementScene) listeElement.get(i));
				ssce.setAffichage(stand.getAffichage());
				ssce.setNumero(ModifierStand.stand.getNumero());
			}
			ModifierStand.stand = ssce;
			this.close();
			break;

		case 5:
			StandPreventionSecours sprev = new StandPreventionSecours(nomText.getText(),
					tailleStand.getSelectionModel().getSelectedItem(), Float.parseFloat(hauteurS.getText()),
					Float.parseFloat(this.longueurS.getText()), Float.parseFloat(largeurS.getText()));
			for (int i = 0; i < listeElement.size(); i++) {
				sprev.ajoutElementPreventionSecour((ElementPreventionSecours) listeElement.get(i));
				sprev.setAffichage(stand.getAffichage());
				sprev.setNumero(ModifierStand.stand.getNumero());
			}
			ModifierStand.stand = sprev;
			this.close();
			break;
		case 6:
			StandConversion sconv = new StandConversion(nomText.getText(),
					tailleStand.getSelectionModel().getSelectedItem(), Float.parseFloat(hauteurS.getText()),
					Float.parseFloat(this.longueurS.getText()), Float.parseFloat(largeurS.getText()));
			sconv.setAffichage(stand.getAffichage());
			sconv.setNumero(ModifierStand.stand.getNumero());
			ModifierStand.stand = sconv;
			this.close();
			break;
		}
	}

	// VIDE LE CONTENU DES TEXTFIELD UNE FOIS UN ELEMENT AJOUTER
	public void enlevercontenu() {
		nom.clear();
		prix.clear();
		quantite.clear();
		nbrobinet.clear();
		nbtoilette.clear();
	}

	public void QuelStand() {

		// "Stand nourriture", "Stand sanitaire",
		// "Stand scene", "Stand prevention", "Stand boutique", "Stand conversion",
		// "Stand boisson"
		if (stand.getNomClasse() == "Stand Nourriture") {
			StandNourriture sn = (StandNourriture) stand;

			nomText.setText(sn.getNom());
			hauteurS.setText(String.valueOf(sn.getHauteur()));
			longueurS.setText(String.valueOf(sn.getLongueur()));
			largeurS.setText(String.valueOf(sn.getLargeur()));
			tailleStand.setValue(sn.getTaille());
			sn.ajouElementNourriture(new ElementNourriture("merguez", 20, 12));
			if (sn.getListeElementsNourritures().size() != 0) {
				for (int i = 0; i < sn.getListeElementsNourritures().size(); i++) {
					repas.add(sn.getListeElementsNourritures().get(i));
				}
			}
			repasAjouter.setText("Repas :");
			repasDispo.setText("Repas Disponibles :");
			nomStand.setSpacing(100);
			listeElementNourriture.setMaxWidth(Double.MAX_VALUE);
			supprRepas.getChildren().remove(0);
			supprRepas.getChildren().add(0, listeElementNourriture);
			HBox.setHgrow(listeElementNourriture, Priority.ALWAYS);
			ajout.setOnAction(e -> {

				ajouterlisteelement(repas, listeElementNourriture, 0);
				enlevercontenu();
			});
			suppr.setOnAction(e -> {
				supprimerlistelement(repas, listeElementNourriture);
			});
			placer.setOnAction(e -> {
				AjoutStand(repas, 0);
			});
		}
		// POUR MODIFIER STAND SANITAIRE
		else if (stand.getNomClasse() == "Stand Sanitaire") {

			StandSanitaire ssan = (StandSanitaire) stand;

			nomText.setText(ssan.getNom());
			hauteurS.setText(String.valueOf(ssan.getHauteur()));
			longueurS.setText(String.valueOf(ssan.getLongueur()));
			largeurS.setText(String.valueOf(ssan.getLargeur()));
			tailleStand.setValue(ssan.getTaille());
			if (ssan.getListeElementsSanitaires().size() != 0) {
				for (int i = 0; i < ssan.getListeElementsSanitaires().size(); i++) {
					Caravanes.add(ssan.getListeElementsSanitaires().get(i));
				}
			}
			BooleanBinding bb3 = new BooleanBinding() {
				{
					super.bind(prix.textProperty(), nom.textProperty(), quantite.textProperty(),
							nbrobinet.textProperty(), nbtoilette.textProperty()

					);
				}

				@Override
				protected boolean computeValue() {
					return (prix.getText().isEmpty() || nom.getText().isEmpty() || quantite.getText().isEmpty()
							|| nbrobinet.getText().isEmpty() || nbtoilette.getText().isEmpty());

				}

			};
			repasAjouter.setText("Caravanes :");
			repasDispo.setText("Caravanes Disponibles :");
			tailleStand.setMaxWidth(Double.MAX_VALUE);
			creerRepas.getChildren().add(3, nbrobinet);
			creerRepas.getChildren().add(4, nbtoilette);

			prix.setMaxWidth(110);
			nom.setMaxWidth(110);
			quantite.setMaxWidth(110);
			nbrobinet.setMaxWidth(110);
			nbtoilette.setMaxWidth(110);
			nbrobinet.setPromptText("Nb robinets");
			nbtoilette.setPromptText("Nb toilettes");
			nomStand.setSpacing(120);
			ajout.disableProperty().bind(bb3);
			listeElementSanitaire.setMaxWidth(Double.MAX_VALUE);
			supprRepas.getChildren().remove(0);
			supprRepas.getChildren().add(0, listeElementSanitaire);
			ModifierStand.this.setWidth(900);
			HBox.setHgrow(listeElementSanitaire, Priority.ALWAYS);
			nomText.setMaxWidth(688);
			ajout.setOnAction(e -> {

				ajouterlisteelement(Caravanes, listeElementSanitaire, 3);
			});
			suppr.setOnAction(e -> {
				supprimerlistelement(Caravanes, listeElementSanitaire);
			});
			placer.setOnAction(e -> {
				AjoutStand(Caravanes, 3);
			});
			tailleStand.setMaxWidth(840);
		} else if (stand.getNomClasse() == "Stand Scene") {
			StandScene ssce = (StandScene) stand;
			nomText.setText(ssce.getNom());
			hauteurS.setText(String.valueOf(ssce.getHauteur()));
			longueurS.setText(String.valueOf(ssce.getLongueur()));
			largeurS.setText(String.valueOf(ssce.getLargeur()));
			tailleStand.setValue(ssce.getTaille());
			if (ssce.getListeElementsScenes().size() != 0) {
				for (int i = 0; i < ssce.getListeElementsScenes().size(); i++) {
					Materiel.add(ssce.getListeElementsScenes().get(i));
				}
			}
			repasAjouter.setText("Materiel :");
			repasDispo.setText("Materiel Disponibles :");
			tailleStand.setMaxWidth(710);
			nomStand.setSpacing(107);
			ModifierStand.this.setWidth(760);
			listeElementScene.setMaxWidth(Double.MAX_VALUE);
			supprRepas.getChildren().remove(0);
			supprRepas.getChildren().add(0, listeElementScene);
			HBox.setHgrow(listeElementScene, Priority.ALWAYS);
			ajout.setOnAction(e -> {

				ajouterlisteelement(Materiel, listeElementScene, 4);
			});
			suppr.setOnAction(e -> {
				supprimerlistelement(Materiel, listeElementScene);
			});
			placer.setOnAction(e -> {
				AjoutStand(Materiel, 4);
			});

		} else if (stand.getNomClasse() == "Stand Prévention Secours") {
			StandPreventionSecours sp = (StandPreventionSecours) stand;

			nomText.setText(sp.getNom());
			hauteurS.setText(String.valueOf(sp.getHauteur()));
			longueurS.setText(String.valueOf(sp.getLongueur()));
			largeurS.setText(String.valueOf(sp.getLargeur()));
			tailleStand.setValue(sp.getTaille());
			if (sp.getListeElementsPreventionSecours().size() != 0) {
				for (int i = 0; i < sp.getListeElementsPreventionSecours().size(); i++) {
					ArticlesPrev.add(sp.getListeElementsPreventionSecours().get(i));
				}
			}
			repasAjouter.setText("Articles :");
			repasDispo.setText("Articles Disponibles :");
			tailleStand.setMaxWidth(703);
			nomStand.setSpacing(107);
			listeElementPrevention.setMaxWidth(Double.MAX_VALUE);
			supprRepas.getChildren().remove(0);
			supprRepas.getChildren().add(0, listeElementPrevention);
			HBox.setHgrow(listeElementPrevention, Priority.ALWAYS);
			ajout.setOnAction(e -> {

				ajouterlisteelement(ArticlesPrev, listeElementPrevention, 5);
			});
			suppr.setOnAction(e -> {
				supprimerlistelement(ArticlesPrev, listeElementPrevention);
			});
			placer.setOnAction(e -> {
				AjoutStand(ArticlesPrev, 5);
			});
		} else if (stand.getNomClasse() == "Stand Boutique") {
			StandBoutique sbou = (StandBoutique) stand;
			nomText.setText(sbou.getNom());
			hauteurS.setText(String.valueOf(sbou.getHauteur()));
			longueurS.setText(String.valueOf(sbou.getLongueur()));
			largeurS.setText(String.valueOf(sbou.getLargeur()));
			tailleStand.setValue(sbou.getTaille());
			if (sbou.getListeElementsBoutique().size() != 0) {
				for (int i = 0; i < sbou.getListeElementsBoutique().size(); i++) {
					ArticlesBoutiques.add(sbou.getListeElementsBoutique().get(i));
				}
			}
			repasAjouter.setText("Articles :");
			repasDispo.setText("Articles Disponibles :");
			tailleStand.setMaxWidth(703);
			nomStand.setSpacing(107);
			supprRepas.getChildren().remove(0);
			supprRepas.getChildren().add(0, listeElementBoutique);
			listeElementBoutique.setMaxWidth(Double.MAX_VALUE);
			HBox.setHgrow(listeElementBoutique, Priority.ALWAYS);
			ajout.setOnAction(e -> {

				ajouterlisteelement(ArticlesBoutiques, listeElementBoutique, 2);
			});
			suppr.setOnAction(e -> {
				supprimerlistelement(ArticlesBoutiques, listeElementBoutique);
			});
			placer.setOnAction(e -> {
				AjoutStand(ArticlesBoutiques, 2);
			});
		} else if (stand.getNomClasse() == "Stand Conversion") {
			StandConversion scon = (StandConversion) stand;
			nomText.setText(scon.getNom());
			hauteurS.setText(String.valueOf(scon.getHauteur()));
			longueurS.setText(String.valueOf(scon.getLongueur()));
			largeurS.setText(String.valueOf(scon.getLargeur()));
			tailleStand.setValue(scon.getTaille());
			repasAjouter.setText("");
			repasDispo.setText("");
			suppr.setDisable(true);
			prix.setDisable(true);
			nom.setDisable(true);
			quantite.setDisable(true);
			tailleStand.setMaxWidth(672);
			nomStand.setSpacing(80);
			nomText.setMaxWidth(560);
			ModifierStand.this.setWidth(730);
			placer.setOnAction(e -> {
				AjoutStand(null, 6);
			});

		} else if (stand.getNomClasse() == "Stand Boisson") {

			StandBoisson sboi = (StandBoisson) stand;
			nomText.setText(sboi.getNom());
			hauteurS.setText(String.valueOf(sboi.getHauteur()));
			longueurS.setText(String.valueOf(sboi.getLongueur()));
			largeurS.setText(String.valueOf(sboi.getLargeur()));
			tailleStand.setValue(sboi.getTaille());
			if (sboi.getListeElementsBoisson().size() != 0) {
				for (int i = 0; i < sboi.getListeElementsBoisson().size(); i++) {
					Boissons.add(sboi.getListeElementsBoisson().get(i));
				}
			}
			repasAjouter.setText("Boisson :");
			repasDispo.setText("Boisson Disponibles :");
			tailleStand.setMaxWidth(705);
			nomStand.setSpacing(107);

			supprRepas.getChildren().remove(0);
			supprRepas.getChildren().add(0, listeElementBoisson);
			listeElementBoisson.setMaxWidth(Double.MAX_VALUE);
			HBox.setHgrow(listeElementBoisson, Priority.ALWAYS);
			ajout.setOnAction(e -> {
				ajouterlisteelement(Boissons, listeElementBoisson, 1);
			});
			suppr.setOnAction(e -> {
				supprimerlistelement(Boissons, listeElementBoisson);
			});
			placer.setOnAction(e -> {
				AjoutStand(Boissons, 1);
			});
		}

	}

}
