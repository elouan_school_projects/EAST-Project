package logiciels;

public class Artiste {
	private String nom;
	private float cachet;
	private String genre;

	// CONSTRUCTEUR
	public Artiste(String nom, float cachet, String genre) {
		this.nom = nom;
		this.cachet = cachet;
		this.genre = genre;
	}

	// Getters et Setters
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public float getCachet() {
		return cachet;
	}

	public void setCachet(float cachet) {
		this.cachet = cachet;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public float calculCout() {
		return cachet;
	}
}
