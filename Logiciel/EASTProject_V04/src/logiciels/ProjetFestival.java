package logiciels;

import java.io.Serializable;
import java.time.LocalDate;

public class ProjetFestival implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nom;
	private LocalDate dateDeb;
	private LocalDate dateFin;
	private float budget;
	private Carte carte;
	private String path;

	// CONSTRUCTEUR

	public ProjetFestival(String nom, LocalDate dateDeb, LocalDate dateFin, float budget, float taille, String emplacement, String path) {
		this.nom = nom;
		this.dateDeb = dateDeb;
		this.dateFin = dateFin;
		this.budget = budget;
		this.carte = new Carte(taille, emplacement);
		this.path = path;
	}

	// GETTERS ET SETTERS

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public LocalDate getDateDeb() {
		return dateDeb;
	}

	public void setDateDeb(LocalDate dateDeb) {
		this.dateDeb = dateDeb;
	}

	public LocalDate getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}

	public float getBudget() {
		return budget;
	}

	public void setBudget(float budget) {
		this.budget = budget;
	}

	public void setCarte(Carte carte) {
		this.carte = carte;
	}

	public Carte getCarte() {
		return this.carte;
	}
	
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	

}
