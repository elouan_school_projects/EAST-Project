package logiciels;

import java.util.ArrayList;
import jfxtras.scene.control.agenda.Agenda;

public class StandScene extends Stand {
	private int nbPers;
	private float prixTechnicien;
	private ArrayList<ElementScene> listeElementsScenes;
	private ArrayList<Representation> listerep;
	private transient Agenda agenda;

	// CONSTRUCTEUR
	public StandScene(String n, String taille, float hauteur, float longueur, float largeur) {
		super(n, taille, hauteur, longueur, largeur);
		this.nbPers = 0;
		this.listeElementsScenes = new ArrayList<ElementScene>();
		listerep = new ArrayList<Representation>();
	}

	// GETTERS ET SETTERS
	public int getNbpers() {
		return nbPers;
	}

	public void setNbpers(int nbpers) {
		this.nbPers = nbpers;
	}

	public float getPrixTechnicien() {
		return prixTechnicien;
	}

	public void setPrixTechnicien(float prixTechnicien) {
		this.prixTechnicien = prixTechnicien;
	}

	public Agenda getAgenda() {
		return agenda;
	}

	// AJOUTER UN ELEMENT DE SCENE AU STAND
	public void ajoutElementScene(ElementScene c) {
		listeElementsScenes.add(c);
		calculCout();
	}

	// MODIFIER UN ELEMENT DE SCENE AU STAND
	public void editerElementScene(ElementScene s) {
		int i = 0;
		boolean trouve = false;
		while ((i <= listeElementsScenes.size()) && (trouve == false)) {
			if (listeElementsScenes.get(i).equals(s)) {
				trouve = true;
			} else {
				i++;
			}
		}
		if (trouve) {
			listeElementsScenes.set(i, s);
		} else {
			System.out.println("Erreur : l'element de scene " + s + "n'existe pas dans la liste");
		}
	}

	// Ajout d'une representation
	public void ajouterRep(Representation r) {
		if (!listerep.contains(r)) {
			listerep.add(r);
		}
	}

	public void enlever(Representation r) {
		if (listerep.contains(r)) {
			listerep.remove(r);
		}
	}

	// SUPPRIMMER UN ELEMENT DE SCENE AU STAND
	public void supprimmerElementScene(ElementScene s) {
		if (listeElementsScenes.contains(s)) {
			listeElementsScenes.remove(s);
		} else {
			System.out.println("Erreur : l'element de scene " + s + "n'existe pas dans la liste");
		}
	}

	public void rafaraichir() {
		agenda = new Agenda();
		agenda.setAllowDragging(false);
		agenda.setAllowResize(false);
		agenda.setCache(false);
		agenda.setScaleShape(false);
		agenda.setEditAppointmentCallback(null);
	}

	// METHODE DE CALCUL DU COUT
	@Override
	public float calculCout() {
		float coutTotal = 0;

		if (listeElementsScenes.isEmpty()) {
			System.out.println("Erreur : pas d'element de scene assign� � ce stand");
		} else {
			for (int i = 0; i < listeElementsScenes.size(); i++) {
				coutTotal += listeElementsScenes.get(i).calculCout();
			}
		}

		return coutTotal + calculcoutTechnicien();
	}

	// COUT TECHNICIENS
	public float calculcoutTechnicien() {
		return prixTechnicien * nbPers;
	}

	// METHODES AFFICHAGE
	public String listerElementsScenes() {
		String s = new String("\n - contenu du stand :\n");
		if (listeElementsScenes.size() == 0) {
			s += "	> vide";
		} else {
			for (int i = 0; i < listeElementsScenes.size(); i++) {
				s += ("	> " + listeElementsScenes.get(i) + "\n");
			}
		}
		return s;
	}

	public String listerrep() {
		String s = new String("\n - Representation :\n");
		if (listerep.size() == 0) {
			s += "	> vide";
		} else {
			for (int i = 0; i < listerep.size(); i++) {
				s += ("	> " + listerep.get(i) + "\n");
			}
		}
		return s;
	}

	@Override
	public String toString() {
		return super.toString() + this.listerElementsScenes() + listerrep();
	}

	public ArrayList<Representation> getListerep() {
		return listerep;
	}

	public void setListerep(ArrayList<Representation> listerep) {
		this.listerep = listerep;
	}

	public ArrayList<ElementScene> getListeElementsScenes() {
		return listeElementsScenes;
	}

	public void setListeElementsScenes(ArrayList<ElementScene> listeElementsScenes) {
		this.listeElementsScenes = listeElementsScenes;
	}

	// METHODE OBTENTION NOM CLASSE
	@Override
	public String getNomClasse() {
		return "Stand Scene";
	}

}
