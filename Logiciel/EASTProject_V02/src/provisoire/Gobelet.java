package provisoire;

public class Gobelet {
	private String type_Gobelet;
	private float prix_gobelet;

	// Constructeur
	public Gobelet(String type_Gobelet, float prix_gobelet) {

		this.type_Gobelet = type_Gobelet;
		this.prix_gobelet = prix_gobelet;
	}

	// GETTERS ET SETTERS
	public String getType_Gobelet() {
		return type_Gobelet;
	}

	public void setType_Gobelet(String type_Gobelet) {
		this.type_Gobelet = type_Gobelet;
	}

	public float getPrix_gobelet() {
		return prix_gobelet;
	}

	public void setPrix_gobelet(float prix_gobelet) {
		this.prix_gobelet = prix_gobelet;
	}

}
