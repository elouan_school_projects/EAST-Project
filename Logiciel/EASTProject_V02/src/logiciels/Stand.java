package logiciels;

public abstract class Stand {
	private static int compteur = 0;
	protected int numero;
	protected String nom;
	protected String taille;
	protected float hauteur;
	protected float longueur;
	protected float largeur;
	protected float cout;



	// CONSTRUCTEUR
	public Stand(String n, String taille, float hauteur, float longueur, float largeur) {
		this.nom = n;
		this.numero = compteur;
		this.taille = taille;
		this.hauteur = hauteur;
		this.longueur = longueur;
		this.largeur = largeur;
		this.cout = 0;
		compteur++;
	}

	// OBTENTION TYPE STAND
	public abstract String getNomClasse();

	// GETTERS ET SETTERS
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getTaille() {
		return taille;
	}

	public void setTaille(String taille) {
		this.taille = taille;
	}

	public float getHauteur() {
		return hauteur;
	}

	public void setHauteur(float hauteur) {
		this.hauteur = hauteur;
	}

	public float getLongueur() {
		return longueur;
	}

	public void setLongueur(float longueur) {
		this.longueur = longueur;
	}

	public float getLargeur() {
		return largeur;
	}

	public void setLargeur(float largeur) {
		this.largeur = largeur;
	}
	
	public float getCout() {
		return cout;
	}

	public void setCout(float cout) {
		this.cout = cout;
	}

	// CALCULER LE COUT DU STAND
	public abstract float calculCout();

}