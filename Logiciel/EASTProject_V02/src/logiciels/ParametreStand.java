package logiciels;

import javafx.scene.paint.Color;

public enum ParametreStand {
	PETIT(100, 100), MOYEN(150, 125), GRAND(200, 150), BOISSON(Color.GREEN), BOUTIQUE(Color.GREEN), CONVERSION(
			Color.GREEN), NOURRITURE(
					Color.GREEN), PREVENTIONSECOURS(Color.GREEN), SANITAIRE(Color.GREEN), SCENE(Color.GREEN);

	private Color couleur;
	private float largeur;
	private float longueur;

	private ParametreStand(Color couleur) {
		this.couleur = couleur;
	}

	private ParametreStand(float largeur, float longueur) {
		this.largeur = largeur;
		this.longueur = longueur;
	}

	public float getLargeur() {
		return largeur;
	}

	public float getLongueur() {
		return longueur;
	}

	public Color getCouleur() {
		return couleur;
	}

}