package logiciels;

public class ElementPreventionSecours extends ElementStand {

	// CONSTRUCTEUR
	public ElementPreventionSecours(String nom, float prixPreventionSecours, int quantite) {
		super(nom, prixPreventionSecours, quantite);
	}

	// METHODE DE CALCUL DU COUT
	public float calculCout() {
		return this.getQuantite() * this.getPrix();
	}

}
