package interfaces;



import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import logiciels.Carte;
import logiciels.Stand;
import logiciels.StandBoisson;

public class FenetreComptabilite {
	private TableView<Stand> tableau = new TableView<Stand>();
	private Label budgetInitial = new Label();
	private Label depenses = new Label("D�penses : ");
	private Label resultat = new Label("R�sultat : �");
	private VBox infos = new VBox();
	private ObservableList<Stand> donnees = FXCollections.observableArrayList();
	

	public FenetreComptabilite(double budget) {
		budgetInitial.setText("Budget initial : "+budget+" �");
		tableau.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		tableau.setEditable(false);
	}

	@SuppressWarnings("unchecked")
	Parent affichage() {
		AnchorPane groot = new AnchorPane();

		
		//CREATION DES COLONNES STAND ET COUT
		TableColumn<Stand, String> stand = new TableColumn<Stand, String>("Stand");
		stand.setCellValueFactory(
				new PropertyValueFactory<Stand, String>("nom"));
		TableColumn<Stand, Float> couts = new TableColumn<Stand, Float>("Co�t");
		couts.setCellValueFactory(
				new PropertyValueFactory<Stand, Float>("cout"));
		StandBoisson s1 = new StandBoisson("Skoll", "50", 10, 20, 30);
		StandBoisson s2 = new StandBoisson("kronembourg", "50", 10, 20, 30);
		
		
		
		tableau.setItems(donnees);
		tableau.getColumns().addAll(stand, couts);
		tableau.minWidth(Double.MAX_VALUE);
		
		
		//CREATION ET PLACEMENT DES INFOS BUDGETAIRES EN DESSOUS DU TABLEAU
		infos.getChildren().addAll(depenses, budgetInitial, resultat);
		VBox.setMargin(depenses, new Insets(5, 10, 5, 150));
		VBox.setMargin(budgetInitial, new Insets(5, 10, 5, 150));
		VBox.setMargin(resultat, new Insets(5, 10, 5, 150));

		//PLACEMENT DU TABLEAU
		AnchorPane.setTopAnchor(tableau, 10.0);
		AnchorPane.setLeftAnchor(tableau, 10.0);
		AnchorPane.setRightAnchor(tableau, 15.0);


		AnchorPane.setTopAnchor(infos, 450.0);
		AnchorPane.setLeftAnchor(infos, 250.0);
		AnchorPane.setRightAnchor(infos, 250.0);
		
		//AJOUT CSS : TAILLE, BORDURE...
		infos.setStyle("-fx-font-size : 30px;"
					 + "-fx-border-width: 1px;" + 
					   "-fx-border-style: solid;" + 
					   "-fx-border-color: lightgrey");
		

		groot.getChildren().addAll(tableau, infos);

		return groot;
	}
	
	//INITIALISATION DU BUDGET
	public void setBudget(double budget) {
		budgetInitial.setText("Budget initial : "+budget+" �");
	}
	
	/*
	public void actuCompta(Carte c) {
		for(int i=0;i<c.getStand().size();i++) {
			c.getStand().get(i).getNom();
		}
		
	}*/
	
}
