package interfaces;

import java.awt.peer.MouseInfoPeer;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.attribute.PosixFilePermission;

import javafx.animation.PauseTransition;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

//import com.sun.javafx.cursor.CursorFrame;
//import com.sun.javafx.cursor.CursorType;
//import com.sun.javafx.cursor.ImageCursorFrame;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.css.PseudoClass;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.ImageCursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.util.Duration;
import logiciels.AffichageStand;
import logiciels.ListeImages;
import logiciels.ProjetFestival;
import logiciels.Stand;
import logiciels.ListeImages;

public class FenetrePrincipale extends Stage implements Serializable {
	private ProjetFestival projet;
	private static final long serialVersionUID = -3165519192693688242L;

	private BorderPane panePrincipal;
	private VBox partieGauche;
	private VBox partieCentrale;

	private MenuBar menuPrincipal;
	private Menu menuFichier;
	private MenuItem nouveau;
	private MenuItem ouvrir;
	private MenuItem enregistrer;
	private MenuItem enregistrerSous;
	private MenuItem quitter;
	private FileChooser selecteurFichier;

	private FenetreHistorique historique;

	private TabPane onglets;
	private Tab ongletStands;
	private Tab ongletArtistes;
	private Tab ongletPlanning;
	private Tab ongletComptabilitee;
	private Tab ongletCarte;

	private HBox menuFoncArtistes;
	private HBox menuFoncStands;
	private Button ajouterStands;
	private Button editerStands;
	private Button supprimerStands;
	private Button tournerStands;
	private Button deplacerStands;
	private Button ajouterArtistes;
	private Button editerArtistes;
	private Button supprimerArtistes;

	private MenuItem optionAjouter;
	private MenuItem optionModifier;
	private MenuItem optionSupprimer;
	private MenuItem optionTourner;
	private MenuItem optionDeplacer;
	private ContextMenu menu;

	private StackPane affichageCarte;
	private Rectangle fondCarte;

	private String urlImage;
	private float largeurImage;
	private float longueurImage;
	private Stand standSelectionne;
	private StackPane imageSelectionne = new StackPane();
	private int compteurClicSelection = 0;
	private double sourisCoordX;
	private double sourisCoordY;

	private FenetreComptabilite compta = new FenetreComptabilite();

	private float largeurEcran;
	private float hauteurEcran;

	public FenetrePrincipale() {
		this.setTitle("EAST");
		this.setX(0);
		this.setY(0);
		largeurEcran = 1300;
		hauteurEcran = 800;
		this.setResizable(false);

		this.initProjet();

		// ------INITIALISATION DES DIFFERENTS ELEMENTS------
		// Groupe principal
		panePrincipal = new BorderPane();
		partieCentrale = new VBox();
		partieGauche = new VBox();
		// Menu
		menuPrincipal = new MenuBar();
		menuFichier = new Menu("Fichier");
		nouveau = new MenuItem("Nouveau Projet");
		ouvrir = new MenuItem("Ouvrir Projet");
		enregistrer = new MenuItem("Enregistrer");
		enregistrerSous = new MenuItem("Enregistrer Sous");
		quitter = new MenuItem("Quitter");
		this.initMenuPrincipal();
		// Historique

		historique = new FenetreHistorique(projet);
		this.initHistorique();

		// Menu Fonctionnalit�es
		menuFoncArtistes = new HBox();
		menuFoncStands = new HBox();
		ajouterStands = new Button("Ajouter Stand");
		editerStands = new Button("Editer Stand");
		supprimerStands = new Button("Supprimer Stand");
		tournerStands = new Button("Tourner Stand");
		deplacerStands = new Button("Deplacer Stand");
		ajouterArtistes = new Button("Ajouter Artiste");
		editerArtistes = new Button("Editer Artiste");
		supprimerArtistes = new Button("Supprimer Artiste");
		selecteurFichier = new FileChooser();
		this.initMenuFonc();

		// Menu Contextuel
		optionAjouter = new MenuItem("Ajouter Stand...");
		optionModifier = new MenuItem("Modifier Stand...");
		optionSupprimer = new MenuItem("Supprimer Stand");
		optionTourner = new MenuItem("Tourner Stand");
		optionDeplacer = new MenuItem("Deplacer Stand");
		menu = new ContextMenu(optionAjouter, optionModifier, optionSupprimer, new SeparatorMenuItem(), optionTourner,
				optionDeplacer);
		this.initMenuContextuel();

		// Carte interactive
		fondCarte = new Rectangle();
		this.initCarte();
		// Onglets
		onglets = new TabPane();
		ongletStands = new Tab();
		ongletArtistes = new Tab();
		ongletPlanning = new Tab();
		ongletComptabilitee = new Tab();
		ongletCarte = ongletStands;
		this.initOnglets();
		// Boutons
		this.initButton();

		// MISE A JOUR DU COMPTEUR
		if (projet.getCarte().getStand().size() != 0) {
			Stand.setCompteur(
					projet.getCarte().getStand().get(projet.getCarte().getStand().size() - 1).getNumero() + 1);
		}

		// ------INITIALISATION DE LA SCENE------
		Scene scene = new Scene(creerContenu(), this.getWidth(), this.getHeight());
		this.setHeight(hauteurEcran);
		this.setWidth(largeurEcran);
		scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
		this.setScene(scene);

		// ------INITIALISATION DES PARAMETRES DES ELEMENTS------
		partieGauche.setPrefWidth(largeurEcran * (2.0 / 10.0));
		partieCentrale.setPrefWidth(largeurEcran * (8.0 / 10.0));
	}

	private void initMenuPrincipal() {
		// Cr�ation du menu principal
		menuPrincipal.getMenus().add(menuFichier);

		selecteurFichier.getExtensionFilters().add(new ExtensionFilter("Fichier projet", "*.ser"));

		// Bouton "Nouveau Projet" du menu principal
		nouveau.setOnAction(e -> {

			CreationFestival creerFestival = new CreationFestival();
			creerFestival.show();

			System.out.println("Nouveau Projet");

		});
		// Bouton "Ouvrir Projet" du menu principal
		ouvrir.setOnAction(e -> {
			FenetrePrincipale fen;
			File fichier = selecteurFichier.showOpenDialog(this);
			if (fichier != null) {
				try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(fichier))) {
					MenuPrincipal.projet = (ProjetFestival) in.readObject();
					fen = new FenetrePrincipale();
					fen.show();
					close();
				} catch (Exception exc) {
					exc.printStackTrace();
				}
			}

			System.out.println("Ouvrir Projet");
		});
		// Bouton "Enregistrer" du menu principal
		enregistrer.setOnAction(e -> {
			sauvegarde(projet);
			System.out.println("Enregistrer");
		});
		// Bouton "Enregistrer Sous" du menu principal
		enregistrerSous.setOnAction(e -> {

			selecteurFichier.setTitle("Enregistrer le festival");
			File fichier = selecteurFichier.showSaveDialog(this);
			if (fichier != null) {
				try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fichier))) {
					projet.setNom(fichier.getName());
					out.writeObject(projet);
				} catch (Exception exc) {
					exc.printStackTrace();
				}
			}
			System.out.println("Enregistrer Sous");
		});
		// Bouton "Quitter" du menu principal
		quitter.setOnAction(e -> {
			System.exit(0);
		});

		// Ajout des boutons au menu principal
		menuFichier.getItems().addAll(nouveau, ouvrir, new SeparatorMenuItem(), enregistrer, enregistrerSous,
				new SeparatorMenuItem(), quitter);
	}

	private void initOnglets() {
		// Cr�ation de l'onglet de gestion des Stands
		VBox boxOngletStands = new VBox();
		boxOngletStands.getChildren().addAll(menuFoncStands, affichageCarte);
		ongletStands.setText("Gestion des Stands");
		ongletStands.setContent(boxOngletStands);
		ongletStands.setClosable(false);

		// Cr�ation de l'onglet de gestion des Artistes
		VBox boxOngletArtistes = new VBox();
		boxOngletArtistes.getChildren().addAll(menuFoncArtistes);
		ongletArtistes.setText("Gestion des Artistes");
		ongletArtistes.setContent(boxOngletArtistes);
		ongletArtistes.setClosable(false);

		// Cr�ation de l'onglet de gestion du Planning
		ongletPlanning.setText("Gestion du Planning");
		ongletPlanning.setContent(new Rectangle(200, 200, Color.YELLOW));
		ongletPlanning.setClosable(false);

		// Cr�ation de l'onglet de gestion de la comptabilit�e

		ongletComptabilitee.setText("Gestion de la Comptabilit�");
		ongletComptabilitee.setContent(compta.affichage(this.projet.getCarte()));
		ongletComptabilitee.setClosable(false);

		// Ajout des onglets dans la liste des onglets
		onglets.getTabs().addAll(ongletStands, ongletArtistes, ongletPlanning, ongletComptabilitee);

		// Gestion du changment d'onglet pour afficher un meme �lement dans un onglet

		onglets.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
			@Override
			public void changed(ObservableValue<? extends Tab> observable, Tab ancientOnglet, Tab nouvelonglet) {
				if ((nouvelonglet == ongletStands) && (ongletCarte == ongletArtistes)) {
					boxOngletArtistes.getChildren().remove(affichageCarte);
					boxOngletStands.getChildren().add(affichageCarte);
					ongletCarte = ongletStands;

				} else if ((nouvelonglet == ongletArtistes) && (ongletCarte == ongletStands)) {
					boxOngletStands.getChildren().remove(affichageCarte);
					boxOngletArtistes.getChildren().add(affichageCarte);
					ongletCarte = ongletArtistes;
				} else if (nouvelonglet == ongletComptabilitee) {
					compta = new FenetreComptabilite();
					compta.setBudget(projet.getBudget());
					ongletComptabilitee.setContent(compta.affichage(projet.getCarte()));
					compta.actuCompta();

				}
			}
		});
	}

	private void initMenuFonc() {
		// Creation Menu Fonctionnalit�es de la gestion des stands
		menuFoncStands.setPadding(new Insets(10));
		menuFoncStands.setSpacing(10);
		menuFoncStands.getChildren().addAll(ajouterStands, editerStands, supprimerStands,
				new Separator(Orientation.VERTICAL), tournerStands, deplacerStands);

		// Creation Menu Fonctionnalit�es de la gestion des artistes
		menuFoncArtistes.setPadding(new Insets(10));
		menuFoncArtistes.setSpacing(10);
		menuFoncArtistes.getChildren().addAll(ajouterArtistes, editerArtistes, supprimerArtistes);
	}

	private void initButton() {
		ajouterStands.setOnAction(e1 -> {
			CreationStand fen = new CreationStand();
			fen.initModality(Modality.WINDOW_MODAL);
			fen.initOwner(this.getScene().getWindow());
			fen.show();
			fen.setOnHidden(ef -> {
				if (!(CreationStand.getStand().getNom().equals(""))
						&& !(CreationStand.getStand().getTaille().equals(""))) {

					if (CreationStand.getStand().getNomClasse().equals("Stand Boisson")) {
						this.getScene().setCursor(new ImageCursor(ListeImages.PETITSTANDBOISSON.getImage()));
						if (CreationStand.getStand().getTaille().equals("Stand Petit")) {
							urlImage = ListeImages.PETITSTANDBOISSON.getUrl();
							largeurImage = ListeImages.PETITSTANDBOISSON.getLargeur();
							longueurImage = ListeImages.PETITSTANDBOISSON.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Moyen")) {
							urlImage = ListeImages.MOYENSTANDBOISSON.getUrl();
							largeurImage = ListeImages.MOYENSTANDBOISSON.getLargeur();
							longueurImage = ListeImages.MOYENSTANDBOISSON.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Grand")) {
							urlImage = ListeImages.GRANDSTANDBOISSON.getUrl();
							largeurImage = ListeImages.GRANDSTANDBOISSON.getLargeur();
							longueurImage = ListeImages.GRANDSTANDBOISSON.getLongueur();
						}
					} else if (CreationStand.getStand().getNomClasse().equals("Stand Boutique")) {
						this.getScene().setCursor(new ImageCursor(ListeImages.PETITSTANDBOUTIQUE.getImage()));
						if (CreationStand.getStand().getTaille().equals("Stand Petit")) {
							urlImage = ListeImages.PETITSTANDBOUTIQUE.getUrl();
							largeurImage = ListeImages.PETITSTANDBOUTIQUE.getLargeur();
							longueurImage = ListeImages.PETITSTANDBOUTIQUE.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Moyen")) {
							urlImage = ListeImages.MOYENSTANDBOUTIQUE.getUrl();
							largeurImage = ListeImages.MOYENSTANDBOUTIQUE.getLargeur();
							longueurImage = ListeImages.MOYENSTANDBOUTIQUE.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Grand")) {
							urlImage = ListeImages.GRANDSTANDBOUTIQUE.getUrl();
							largeurImage = ListeImages.GRANDSTANDBOUTIQUE.getLargeur();
							longueurImage = ListeImages.GRANDSTANDBOUTIQUE.getLongueur();
						}
					} else if (CreationStand.getStand().getNomClasse().equals("Stand Conversion")) {
						this.getScene().setCursor(new ImageCursor(ListeImages.PETITSTANDCONVERSION.getImage()));
						if (CreationStand.getStand().getTaille().equals("Stand Petit")) {
							urlImage = ListeImages.PETITSTANDCONVERSION.getUrl();
							largeurImage = ListeImages.PETITSTANDCONVERSION.getLargeur();
							longueurImage = ListeImages.PETITSTANDCONVERSION.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Moyen")) {
							urlImage = ListeImages.MOYENSTANDCONVERSION.getUrl();
							largeurImage = ListeImages.MOYENSTANDCONVERSION.getLargeur();
							longueurImage = ListeImages.MOYENSTANDCONVERSION.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Grand")) {
							urlImage = ListeImages.GRANDSTANDCONVERSION.getUrl();
							largeurImage = ListeImages.GRANDSTANDCONVERSION.getLargeur();
							longueurImage = ListeImages.GRANDSTANDCONVERSION.getLongueur();
						}
					} else if (CreationStand.getStand().getNomClasse().equals("Stand Nourriture")) {
						this.getScene().setCursor(new ImageCursor(ListeImages.PETITSTANDNOURRITURE.getImage()));
						if (CreationStand.getStand().getTaille().equals("Stand Petit")) {
							urlImage = ListeImages.PETITSTANDNOURRITURE.getUrl();
							largeurImage = ListeImages.PETITSTANDNOURRITURE.getLargeur();
							longueurImage = ListeImages.PETITSTANDNOURRITURE.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Moyen")) {
							urlImage = ListeImages.MOYENSTANDNOURRITURE.getUrl();
							largeurImage = ListeImages.MOYENSTANDNOURRITURE.getLargeur();
							longueurImage = ListeImages.MOYENSTANDNOURRITURE.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Grand")) {
							urlImage = ListeImages.GRANDSTANDNOURRITURE.getUrl();
							largeurImage = ListeImages.GRANDSTANDNOURRITURE.getLargeur();
							longueurImage = ListeImages.GRANDSTANDNOURRITURE.getLongueur();
						}
					} else if (CreationStand.getStand().getNomClasse().equals("Stand Pr�vention Secours")) {
						this.getScene().setCursor(new ImageCursor(ListeImages.PETITSTANDPREVENTIONSECOURS.getImage()));
						if (CreationStand.getStand().getTaille().equals("Stand Petit")) {
							urlImage = ListeImages.PETITSTANDPREVENTIONSECOURS.getUrl();
							largeurImage = ListeImages.PETITSTANDPREVENTIONSECOURS.getLargeur();
							longueurImage = ListeImages.PETITSTANDPREVENTIONSECOURS.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Moyen")) {
							urlImage = ListeImages.MOYENSTANDPREVENTIONSECOURS.getUrl();
							largeurImage = ListeImages.MOYENSTANDPREVENTIONSECOURS.getLargeur();
							longueurImage = ListeImages.MOYENSTANDPREVENTIONSECOURS.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Grand")) {
							urlImage = ListeImages.GRANDSTANDPREVENTIONSECOURS.getUrl();
							largeurImage = ListeImages.GRANDSTANDPREVENTIONSECOURS.getLargeur();
							longueurImage = ListeImages.GRANDSTANDPREVENTIONSECOURS.getLongueur();
						}
					} else if (CreationStand.getStand().getNomClasse().equals("Stand Sanitaire")) {
						this.getScene().setCursor(new ImageCursor(ListeImages.PETITSTANDSANITAIRE.getImage()));
						if (CreationStand.getStand().getTaille().equals("Stand Petit")) {
							urlImage = ListeImages.PETITSTANDSANITAIRE.getUrl();
							largeurImage = ListeImages.PETITSTANDSANITAIRE.getLargeur();
							longueurImage = ListeImages.PETITSTANDSANITAIRE.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Moyen")) {
							urlImage = ListeImages.MOYENSTANDSANITAIRE.getUrl();
							largeurImage = ListeImages.MOYENSTANDSANITAIRE.getLargeur();
							longueurImage = ListeImages.MOYENSTANDSANITAIRE.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Grand")) {
							urlImage = ListeImages.GRANDSTANDSANITAIRE.getUrl();
							largeurImage = ListeImages.GRANDSTANDSANITAIRE.getLargeur();
							longueurImage = ListeImages.GRANDSTANDSANITAIRE.getLongueur();
						}
					} else if (CreationStand.getStand().getNomClasse().equals("Stand Scene")) {
						this.getScene().setCursor(new ImageCursor(ListeImages.PETITSTANDSCENE.getImage()));
						if (CreationStand.getStand().getTaille().equals("Stand Petit")) {
							urlImage = ListeImages.PETITSTANDSCENE.getUrl();
							largeurImage = ListeImages.PETITSTANDSCENE.getLargeur();
							longueurImage = ListeImages.PETITSTANDSCENE.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Moyen")) {
							urlImage = ListeImages.MOYENSTANDSCENE.getUrl();
							largeurImage = ListeImages.MOYENSTANDSCENE.getLargeur();
							longueurImage = ListeImages.MOYENSTANDSCENE.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Grand")) {
							urlImage = ListeImages.GRANDSTANDSCENE.getUrl();
							largeurImage = ListeImages.GRANDSTANDSCENE.getLargeur();
							longueurImage = ListeImages.GRANDSTANDSCENE.getLongueur();
						}
					}

					affichageCarte.setOnMouseClicked(ec -> {
						if ((ec.getButton() == MouseButton.PRIMARY)
								&& (this.getScene().getCursor() != Cursor.DEFAULT)) {
							CreationStand.getStand().setAffichage(new AffichageStand(ec.getX(), ec.getY(),
									this.urlImage, this.largeurImage, this.longueurImage, affichageCarte));
							projet.getCarte().ajoutStand(CreationStand.getStand());
							this.updateHistorique();

							for (Stand s : projet.getCarte().getStand()) {
								if (CreationStand.getStand().getNumero() == s.getNumero()) {
									ImageView im = s.getAffichage().getImage();
									im.setUserData(s.getNumero());

									affichageCarte.getChildren().add(im);
								}
							}
							this.initStand();
							this.getScene().setCursor(Cursor.DEFAULT);
						} else if (ec.getButton() == MouseButton.SECONDARY) {
							this.getScene().setCursor(Cursor.DEFAULT);
						}
					});
				}
			});
		});
		supprimerStands.setOnAction(e1 -> {
			if (standSelectionne != null) {
				affichageCarte.getChildren().remove(imageSelectionne);
				projet.getCarte().supprimmerStand(standSelectionne);
				optionModifier.setDisable(true);
				optionSupprimer.setDisable(true);
				optionTourner.setDisable(true);
				optionDeplacer.setDisable(true);
				supprimerStands.setDisable(true);
				editerStands.setDisable(true);
				tournerStands.setDisable(true);
				deplacerStands.setDisable(true);
				standSelectionne = null;
				this.updateHistorique();
				compteurClicSelection = 0;
			}
		});
		/*
		 * editerStands.setOnAction(e1 -> { if (standSelectionne != null) {
		 * CreationStand fen = new EditionStand();
		 * fen.initModality(Modality.WINDOW_MODAL);
		 * fen.initOwner(this.getScene().getWindow()); fen.show(); } });
		 */
		tournerStands.setOnAction(e -> {
			if (standSelectionne != null) {
				affichageCarte.getChildren().add(imageSelectionne.getChildren().get(1));
				affichageCarte.getChildren().remove(imageSelectionne);
				double rotation = affichageCarte.getChildren().get(affichageCarte.getChildren().size() - 1).getRotate();
				affichageCarte.getChildren().get(affichageCarte.getChildren().size() - 1).setRotate(rotation + 90);
				for (int i = 0; i < projet.getCarte().getStand().size(); i++) {
					if (standSelectionne.getNumero() == projet.getCarte().getStand().get(i).getNumero()) {
						float temp = standSelectionne.getAffichage().getLargeur();
						projet.getCarte().getStand().get(i).getAffichage()
								.setLargeur(standSelectionne.getAffichage().getLongueur());
						projet.getCarte().getStand().get(i).getAffichage().setLongueur(temp);
					}
				}
				Rectangle r = new Rectangle(standSelectionne.getAffichage().getLargeur(),
						standSelectionne.getAffichage().getLongueur(), Color.TRANSPARENT);
				r.setStroke(Color.WHITE);
				r.setStrokeWidth(2);
				r.setArcHeight(5);
				r.setArcWidth(5);
				r.setTranslateX(
						affichageCarte.getChildren().get(affichageCarte.getChildren().size() - 1).getTranslateX());
				r.setTranslateY(
						affichageCarte.getChildren().get(affichageCarte.getChildren().size() - 1).getTranslateY());
				imageSelectionne = new StackPane(r,
						affichageCarte.getChildren().get(affichageCarte.getChildren().size() - 1));
				imageSelectionne.setUserData(
						affichageCarte.getChildren().get(affichageCarte.getChildren().size() - 1).getUserData());
				affichageCarte.getChildren().add(imageSelectionne);
			}
		});
	}

	private void initMenuContextuel() {
		optionModifier.setDisable(true);
		optionSupprimer.setDisable(true);
		optionTourner.setDisable(true);
		optionDeplacer.setDisable(true);
		supprimerStands.setDisable(true);
		editerStands.setDisable(true);
		tournerStands.setDisable(true);
		deplacerStands.setDisable(true);
		optionAjouter.setOnAction(e -> {
			CreationStand fen = new CreationStand();
			fen.initModality(Modality.WINDOW_MODAL);
			fen.initOwner(this.getScene().getWindow());
			fen.show();
			fen.setOnHidden(ef -> {
				if (!(CreationStand.getStand().getNom().equals(""))
						&& !(CreationStand.getStand().getTaille().equals(""))) {

					if (CreationStand.getStand().getNomClasse().equals("Stand Boisson")) {
						this.getScene().setCursor(new ImageCursor(ListeImages.PETITSTANDBOISSON.getImage()));
						if (CreationStand.getStand().getTaille().equals("Stand Petit")) {
							urlImage = ListeImages.PETITSTANDBOISSON.getUrl();
							largeurImage = ListeImages.PETITSTANDBOISSON.getLargeur();
							longueurImage = ListeImages.PETITSTANDBOISSON.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Moyen")) {
							urlImage = ListeImages.MOYENSTANDBOISSON.getUrl();
							largeurImage = ListeImages.MOYENSTANDBOISSON.getLargeur();
							longueurImage = ListeImages.MOYENSTANDBOISSON.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Grand")) {
							urlImage = ListeImages.GRANDSTANDBOISSON.getUrl();
							largeurImage = ListeImages.GRANDSTANDBOISSON.getLargeur();
							longueurImage = ListeImages.GRANDSTANDBOISSON.getLongueur();
						}
					} else if (CreationStand.getStand().getNomClasse().equals("Stand Boutique")) {
						this.getScene().setCursor(new ImageCursor(ListeImages.PETITSTANDBOUTIQUE.getImage()));
						if (CreationStand.getStand().getTaille().equals("Stand Petit")) {
							urlImage = ListeImages.PETITSTANDBOUTIQUE.getUrl();
							largeurImage = ListeImages.PETITSTANDBOUTIQUE.getLargeur();
							longueurImage = ListeImages.PETITSTANDBOUTIQUE.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Moyen")) {
							urlImage = ListeImages.MOYENSTANDBOUTIQUE.getUrl();
							largeurImage = ListeImages.MOYENSTANDBOUTIQUE.getLargeur();
							longueurImage = ListeImages.MOYENSTANDBOUTIQUE.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Grand")) {
							urlImage = ListeImages.GRANDSTANDBOUTIQUE.getUrl();
							largeurImage = ListeImages.GRANDSTANDBOUTIQUE.getLargeur();
							longueurImage = ListeImages.GRANDSTANDBOUTIQUE.getLongueur();
						}
					} else if (CreationStand.getStand().getNomClasse().equals("Stand Conversion")) {
						this.getScene().setCursor(new ImageCursor(ListeImages.PETITSTANDCONVERSION.getImage()));
						if (CreationStand.getStand().getTaille().equals("Stand Petit")) {
							urlImage = ListeImages.PETITSTANDCONVERSION.getUrl();
							largeurImage = ListeImages.PETITSTANDCONVERSION.getLargeur();
							longueurImage = ListeImages.PETITSTANDCONVERSION.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Moyen")) {
							urlImage = ListeImages.MOYENSTANDCONVERSION.getUrl();
							largeurImage = ListeImages.MOYENSTANDCONVERSION.getLargeur();
							longueurImage = ListeImages.MOYENSTANDCONVERSION.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Grand")) {
							urlImage = ListeImages.GRANDSTANDCONVERSION.getUrl();
							largeurImage = ListeImages.GRANDSTANDCONVERSION.getLargeur();
							longueurImage = ListeImages.GRANDSTANDCONVERSION.getLongueur();
						}
					} else if (CreationStand.getStand().getNomClasse().equals("Stand Nourriture")) {
						this.getScene().setCursor(new ImageCursor(ListeImages.PETITSTANDNOURRITURE.getImage()));
						if (CreationStand.getStand().getTaille().equals("Stand Petit")) {
							urlImage = ListeImages.PETITSTANDNOURRITURE.getUrl();
							largeurImage = ListeImages.PETITSTANDNOURRITURE.getLargeur();
							longueurImage = ListeImages.PETITSTANDNOURRITURE.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Moyen")) {
							urlImage = ListeImages.MOYENSTANDNOURRITURE.getUrl();
							largeurImage = ListeImages.MOYENSTANDNOURRITURE.getLargeur();
							longueurImage = ListeImages.MOYENSTANDNOURRITURE.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Grand")) {
							urlImage = ListeImages.GRANDSTANDNOURRITURE.getUrl();
							largeurImage = ListeImages.GRANDSTANDNOURRITURE.getLargeur();
							longueurImage = ListeImages.GRANDSTANDNOURRITURE.getLongueur();
						}
					} else if (CreationStand.getStand().getNomClasse().equals("Stand Pr�vention Secours")) {
						this.getScene().setCursor(new ImageCursor(ListeImages.PETITSTANDPREVENTIONSECOURS.getImage()));
						if (CreationStand.getStand().getTaille().equals("Stand Petit")) {
							urlImage = ListeImages.PETITSTANDPREVENTIONSECOURS.getUrl();
							largeurImage = ListeImages.PETITSTANDPREVENTIONSECOURS.getLargeur();
							longueurImage = ListeImages.PETITSTANDPREVENTIONSECOURS.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Moyen")) {
							urlImage = ListeImages.MOYENSTANDPREVENTIONSECOURS.getUrl();
							largeurImage = ListeImages.MOYENSTANDPREVENTIONSECOURS.getLargeur();
							longueurImage = ListeImages.MOYENSTANDPREVENTIONSECOURS.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Grand")) {
							urlImage = ListeImages.GRANDSTANDPREVENTIONSECOURS.getUrl();
							largeurImage = ListeImages.GRANDSTANDPREVENTIONSECOURS.getLargeur();
							longueurImage = ListeImages.GRANDSTANDPREVENTIONSECOURS.getLongueur();
						}
					} else if (CreationStand.getStand().getNomClasse().equals("Stand Sanitaire")) {
						this.getScene().setCursor(new ImageCursor(ListeImages.PETITSTANDSANITAIRE.getImage()));
						if (CreationStand.getStand().getTaille().equals("Stand Petit")) {
							urlImage = ListeImages.PETITSTANDSANITAIRE.getUrl();
							largeurImage = ListeImages.PETITSTANDSANITAIRE.getLargeur();
							longueurImage = ListeImages.PETITSTANDSANITAIRE.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Moyen")) {
							urlImage = ListeImages.MOYENSTANDSANITAIRE.getUrl();
							largeurImage = ListeImages.MOYENSTANDSANITAIRE.getLargeur();
							longueurImage = ListeImages.MOYENSTANDSANITAIRE.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Grand")) {
							urlImage = ListeImages.GRANDSTANDSANITAIRE.getUrl();
							largeurImage = ListeImages.GRANDSTANDSANITAIRE.getLargeur();
							longueurImage = ListeImages.GRANDSTANDSANITAIRE.getLongueur();
						}
					} else if (CreationStand.getStand().getNomClasse().equals("Stand Scene")) {
						this.getScene().setCursor(new ImageCursor(ListeImages.PETITSTANDSCENE.getImage()));
						if (CreationStand.getStand().getTaille().equals("Stand Petit")) {
							urlImage = ListeImages.PETITSTANDSCENE.getUrl();
							largeurImage = ListeImages.PETITSTANDSCENE.getLargeur();
							longueurImage = ListeImages.PETITSTANDSCENE.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Moyen")) {
							urlImage = ListeImages.MOYENSTANDSCENE.getUrl();
							largeurImage = ListeImages.MOYENSTANDSCENE.getLargeur();
							longueurImage = ListeImages.MOYENSTANDSCENE.getLongueur();
						} else if (CreationStand.getStand().getTaille().equals("Stand Grand")) {
							urlImage = ListeImages.GRANDSTANDSCENE.getUrl();
							largeurImage = ListeImages.GRANDSTANDSCENE.getLargeur();
							longueurImage = ListeImages.GRANDSTANDSCENE.getLongueur();
						}
					}
					affichageCarte.setOnMouseClicked(ec -> {
						if ((ec.getButton() == MouseButton.PRIMARY)
								&& (this.getScene().getCursor() != Cursor.DEFAULT)) {
							CreationStand.getStand().setAffichage(new AffichageStand(ec.getX(), ec.getY(),
									this.urlImage, this.largeurImage, this.longueurImage, affichageCarte));
							projet.getCarte().ajoutStand(CreationStand.getStand());
							this.updateHistorique();

							for (Stand s : projet.getCarte().getStand()) {
								if (CreationStand.getStand().getNumero() == s.getNumero()) {
									ImageView im = s.getAffichage().getImage();
									im.setUserData(s.getNumero());

									affichageCarte.getChildren().add(im);
								}
							}
							this.initStand();
							this.getScene().setCursor(Cursor.DEFAULT);
						} else if (ec.getButton() == MouseButton.SECONDARY) {
							this.getScene().setCursor(Cursor.DEFAULT);
						}
					});
				}
			});
		});
		optionModifier.setOnAction(e -> {

		});
		optionSupprimer.setOnAction(e -> {
			if (standSelectionne != null) {
				affichageCarte.getChildren().remove(imageSelectionne);
				projet.getCarte().supprimmerStand(standSelectionne);
				optionModifier.setDisable(true);
				optionSupprimer.setDisable(true);
				optionTourner.setDisable(true);
				optionDeplacer.setDisable(true);
				supprimerStands.setDisable(true);
				editerStands.setDisable(true);
				tournerStands.setDisable(true);
				deplacerStands.setDisable(true);
				standSelectionne = null;
				this.updateHistorique();
				compteurClicSelection = 0;
			}
		});
		optionTourner.setOnAction(e -> {
			if (standSelectionne != null) {
				affichageCarte.getChildren().add(imageSelectionne.getChildren().get(1));
				affichageCarte.getChildren().remove(imageSelectionne);
				double rotation = affichageCarte.getChildren().get(affichageCarte.getChildren().size() - 1).getRotate();
				affichageCarte.getChildren().get(affichageCarte.getChildren().size() - 1).setRotate(rotation + 90);
				for (int i = 0; i < projet.getCarte().getStand().size(); i++) {
					if (standSelectionne.getNumero() == projet.getCarte().getStand().get(i).getNumero()) {
						float temp = standSelectionne.getAffichage().getLargeur();
						projet.getCarte().getStand().get(i).getAffichage()
								.setLargeur(standSelectionne.getAffichage().getLongueur());
						projet.getCarte().getStand().get(i).getAffichage().setLongueur(temp);
					}
				}
				Rectangle r = new Rectangle(standSelectionne.getAffichage().getLargeur(),
						standSelectionne.getAffichage().getLongueur(), Color.TRANSPARENT);
				r.setStroke(Color.WHITE);
				r.setStrokeWidth(2);
				r.setArcHeight(5);
				r.setArcWidth(5);
				r.setTranslateX(
						affichageCarte.getChildren().get(affichageCarte.getChildren().size() - 1).getTranslateX());
				r.setTranslateY(
						affichageCarte.getChildren().get(affichageCarte.getChildren().size() - 1).getTranslateY());
				imageSelectionne = new StackPane(r,
						affichageCarte.getChildren().get(affichageCarte.getChildren().size() - 1));
				imageSelectionne.setUserData(
						affichageCarte.getChildren().get(affichageCarte.getChildren().size() - 1).getUserData());
				affichageCarte.getChildren().add(imageSelectionne);
			}
		});
		optionDeplacer.setOnAction(e -> {
			for (int i = 0; i < projet.getCarte().getStand().size(); i++) {
				if (standSelectionne.getNumero() == projet.getCarte().getStand().get(i).getNumero()) {

					double a = sourisCoordX - projet.getCarte().getStand().get(i).getAffichage().getCoordX();
					double b = sourisCoordX - projet.getCarte().getStand().get(i).getAffichage().getCoordY();
					System.out.println(a + "  ||  " + b);
					projet.getCarte().getStand().get(i).getAffichage().setCoordX(sourisCoordX);
					projet.getCarte().getStand().get(i).getAffichage().setCoordY(sourisCoordY);
					for (int j = 1; j < affichageCarte.getChildren().size(); j++) {
						System.out.println(j);
						if (standSelectionne.getNumero() == (int) affichageCarte.getChildren().get(j).getUserData()) {
							affichageCarte.getChildren().get(j).setTranslateX(a);
							affichageCarte.getChildren().get(j).setTranslateX(b);
						}
					}
				}
			}
		});
	}

	private void initHistorique() {
		// historique.getTreeView().getSele .setOnAction(e -> {

	}

	private void updateHistorique() {
		historique = new FenetreHistorique(projet);
		partieGauche.getChildren().remove(1);
		partieGauche.getChildren().add(1, historique.affichage());
	}

	private void initStand() {
		affichageCarte.setOnMouseClicked(ec -> {
			if ((ec.getButton() == MouseButton.SECONDARY)) {
				if (menu.isShowing()) {
					menu.hide();
				} else {
					menu.show(affichageCarte, ec.getScreenX(), ec.getScreenY());
				}

			} else if (ec.getButton() == MouseButton.PRIMARY) {
				if (menu.isShowing()) {
					menu.hide();
				}
				if (standSelectionne != null) {
					compteurClicSelection++;
					if (compteurClicSelection >= 2) {
						compteurClicSelection = 0;
						affichageCarte.getChildren().add(imageSelectionne.getChildren().get(1));
						affichageCarte.getChildren().remove(imageSelectionne);
						standSelectionne = null;
						optionModifier.setDisable(true);
						optionSupprimer.setDisable(true);
						optionTourner.setDisable(true);
						optionDeplacer.setDisable(true);
						supprimerStands.setDisable(true);
						editerStands.setDisable(true);
						tournerStands.setDisable(true);
						deplacerStands.setDisable(true);
					}
				}
			}
		});

		affichageCarte.setOnMouseMoved(ec -> {
			sourisCoordX = ec.getX();
			sourisCoordY = ec.getY();
			// System.out.println(sourisCoordX + " || " + sourisCoordY);
		});

		affichageCarte.getChildren().forEach(child -> {
			child.setOnMousePressed(e -> {
				if (e.getSource() != fondCarte) {
					if ((e.getButton() == MouseButton.PRIMARY) && (standSelectionne == null)) {
						for (Stand s : projet.getCarte().getStand()) {
							if ((int) child.getUserData() == s.getNumero()) {
								standSelectionne = s;
							}
						}
						Rectangle r = new Rectangle(standSelectionne.getAffichage().getLargeur(),
								standSelectionne.getAffichage().getLongueur(), Color.TRANSPARENT);
						r.setStroke(Color.WHITE);
						r.setStrokeWidth(2);
						r.setArcHeight(5);
						r.setArcWidth(5);
						r.setTranslateX(child.getTranslateX());
						r.setTranslateY(child.getTranslateY());
						imageSelectionne = new StackPane(r, child);
						imageSelectionne.setUserData(child.getUserData());
						affichageCarte.getChildren().add(imageSelectionne);
						optionModifier.setDisable(false);
						optionSupprimer.setDisable(false);
						optionTourner.setDisable(false);
						optionDeplacer.setDisable(false);
						supprimerStands.setDisable(false);
						editerStands.setDisable(false);
						tournerStands.setDisable(false);
						deplacerStands.setDisable(false);
					}
				}
			});

		});

	}

	private void initCarte() {
		affichageCarte = new StackPane();
		fondCarte = new Rectangle(largeurEcran, hauteurEcran, Color.rgb(57, 178, 60));
		affichageCarte.getChildren().add(fondCarte);
		for (Stand s : projet.getCarte().getStand()) {
			ImageView im = s.getAffichage().getImage();
			im.setUserData(s.getNumero());
			affichageCarte.getChildren().remove(im);
			affichageCarte.getChildren().add(im);
			this.initStand();
		}
	}

	private void initProjet() {
		projet = MenuPrincipal.projet;
		this.setTitle(projet.getNom());

	}

	public void setProjetFestival(ProjetFestival projet) {
		this.projet = projet;
	}

	public Parent creerContenu() {
		Group root = new Group();
		// ------PARTIE GAUCHE------
		partieGauche.getChildren().addAll(menuPrincipal, historique.affichage());
		panePrincipal.setLeft(partieGauche);

		// ------PARTIE CENTRALE------
		partieCentrale.getChildren().addAll(onglets);
		panePrincipal.setCenter(partieCentrale);

		root.getChildren().add(panePrincipal);

		return root;
	}

	public void sauvegarde(ProjetFestival projet) {
		try {
			FileOutputStream fichierSortant = new FileOutputStream("sauvegardes/" + projet.getNom() + ".ser");
			ObjectOutputStream save = new ObjectOutputStream(fichierSortant);
			save.writeObject(projet);
			save.close();
			fichierSortant.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}