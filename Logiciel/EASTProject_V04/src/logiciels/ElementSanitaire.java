package logiciels;

public class ElementSanitaire extends ElementStand {
	private int nbToilette;
	private int nbRobinet;

	// CONSTRUCTEUR
	public ElementSanitaire(String nom, float prixElementSanitaire, int quantite, int nbToilette, int nbRobinet) {
		super(nom, prixElementSanitaire, quantite);
		this.nbToilette = nbToilette;
		this.nbRobinet = nbRobinet;

	}

	// GETTERS ET SETTERS
	public int getNbToilette() {
		return nbToilette;
	}

	public void setNbToilette(int nbToilette) {
		this.nbToilette = nbToilette;
	}

	public int getNbRobinet() {
		return nbRobinet;
	}

	public void setNbRobinet(int nbRobinet) {
		this.nbRobinet = nbRobinet;
	}

	// METHODE DE CALCUL DU COUT
	public float calculCout() {
		return this.getQuantite() * this.getPrix();
	}

	@Override
	public String toString() {
		return super.toString() + " nombre toilettes : " + this.nbToilette + " nombre toilettes : " + this.nbRobinet;
	}

}
