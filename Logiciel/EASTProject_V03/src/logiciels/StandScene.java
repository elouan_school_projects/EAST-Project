package logiciels;

import java.util.ArrayList;

public class StandScene extends Stand {
	private int nbPers;
	private float prixTechnicien;
	private ArrayList<ElementScene> listeElementsScenes;
	private ArrayList<Artiste> listeArtistes;

	// CONSTRUCTEUR
	public StandScene(String n, String taille, float hauteur, float longueur, float largeur) {
		super(n, taille, hauteur, longueur, largeur);
		this.nbPers = 0;
		this.listeElementsScenes = new ArrayList<ElementScene>();
		this.listeArtistes = new ArrayList<Artiste>();
	}

	// GETTERS ET SETTERS
	public int getNbpers() {
		return nbPers;
	}

	public void setNbpers(int nbpers) {
		this.nbPers = nbpers;
	}

	public float getPrixTechnicien() {
		return prixTechnicien;
	}

	public void setPrixTechnicien(float prixTechnicien) {
		this.prixTechnicien = prixTechnicien;
	}

	// AJOUTER UN ELEMENT DE SCENE AU STAND
	public void ajoutElementScene(ElementScene c) {
		listeElementsScenes.add(c);
	}

	// MODIFIER UN ELEMENT DE SCENE AU STAND
	public void editerElementScene(ElementScene s) {
		int i = 0;
		boolean trouve = false;
		while ((i <= listeElementsScenes.size()) && (trouve == false)) {
			if (listeElementsScenes.get(i).equals(s)) {
				trouve = true;
			} else {
				i++;
			}
		}
		if (trouve) {
			listeElementsScenes.set(i, s);
		} else {
			System.out.println("Erreur : l'element de scene " + s + "n'existe pas dans la liste");
		}
	}

	// SUPPRIMMER UN ELEMENT DE SCENE AU STAND
	public void supprimmerElementScene(ElementScene s) {
		if (listeElementsScenes.contains(s)) {
			listeElementsScenes.remove(s);
		} else {
			System.out.println("Erreur : l'element de scene " + s + "n'existe pas dans la liste");
		}
	}

	// AJOUTER UN ARTISTE AU STAND
	public void ajoutArtiste(Artiste a) {
		if (listeArtistes.contains(a)) {
			System.out.println("Erreur : l'artiste existe d�j� dans la liste");
		} else {
			listeArtistes.add(a);

		}
	}

	// MODIFIER UN ARTISTE DU STAND
	public void editerArtiste(Artiste a) {
		int i = 0;
		boolean trouve = false;
		while ((i <= listeArtistes.size()) && (trouve == false)) {
			if (listeArtistes.get(i).equals(a)) {
				trouve = true;
			} else {
				i++;
			}
		}
		if (trouve) {
			listeArtistes.set(i, a);
		} else {
			System.out.println("Erreur : l'artiste  " + a + " n'existe pas dans la liste");
		}
	}

	// SUPPRIMMER UN ARTISTE DU STAND
	public void supprimmerArtiste(Artiste a) {
		if (listeArtistes.contains(a)) {
			listeArtistes.remove(a);
		} else {
			System.out.println("Erreur : l'artiste " + a + " n'existe pas dans la liste");
		}
	}

	// METHODE DE CALCUL DU COUT
	@Override
	public float calculCout() {
		float coutTotal = 0;

		if (listeElementsScenes.isEmpty()) {
			System.out.println("Erreur : pas d'element de scene assign� � ce stand");
		} else {
			for (int i = 0; i < listeElementsScenes.size(); i++) {
				coutTotal += listeElementsScenes.get(i).calculCout();
			}
		}

		if (listeArtistes.isEmpty()) {
			System.out.println("Erreur pas d'artiste assign� � ce stand");
		} else {
			for (int i = 0; i < listeArtistes.size(); i++) {
				coutTotal += listeArtistes.get(i).calculCout();
			}
		}
		cout += coutTotal + calculcoutTechnicien();
		return cout;
	}

	// COUT TECHNICIENS
	public float calculcoutTechnicien() {
		return prixTechnicien * nbPers;
	}

	// METHODES AFFICHAGE
	public String listerElementsScenes() {
		String s = new String("\n - contenu du stand :\n");
		if (listeElementsScenes.size() == 0) {
			s += "	> vide";
		} else {
			for (int i = 0; i < listeElementsScenes.size(); i++) {
				s += ("	> " + listeElementsScenes.get(i) + "\n");
			}
		}
		return s;
	}

	@Override
	public String toString() {
		return super.toString() + this.listerElementsScenes();
	}

	// METHODE OBTENTION NOM CLASSE
	@Override
	public String getNomClasse() {
		return "Stand Scene";
	}

}
