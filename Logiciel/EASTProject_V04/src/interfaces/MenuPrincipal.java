package interfaces;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import logiciels.ProjetFestival;

public class MenuPrincipal extends Stage {
	
	static ProjetFestival projet;
	
	private Button creer 	= new Button("Creer un nouveau projet");
	private Button charger 	= new Button("Charger un projet existant");
	private Button quitter 	= new Button("Quitter");

	public MenuPrincipal() {
		this.setTitle("Menu principal");
		this.setX(500);
		this.setY(280);
		this.setResizable(false);

		Scene laScene = new Scene(affichage(), 800, 500);
		this.setScene(laScene);
		this.sizeToScene();
	}

	Parent affichage() {
		BorderPane groot = new BorderPane();
		groot.setTop(creer);
		groot.setCenter(charger);
		groot.setBottom(quitter);
		groot.getStylesheets().add("interfaces/style.css");

		BorderPane.setAlignment(creer, Pos.CENTER);
		BorderPane.setAlignment(charger, Pos.CENTER);
		BorderPane.setAlignment(quitter, Pos.CENTER);
		BorderPane.setMargin(creer, new Insets(20));
		BorderPane.setMargin(charger, new Insets(20));
		BorderPane.setMargin(quitter, new Insets(20));

		creer.setId("btnMenuPrincipal");
		charger.setId("btnMenuPrincipal");
		quitter.setId("btnMenuPrincipal");
		creer.setMaxWidth(Double.MAX_VALUE);
		creer.setMinHeight(120.0);
		charger.setMaxWidth(Double.MAX_VALUE);
		charger.setMinHeight(120.0);
		quitter.setMaxWidth(Double.MAX_VALUE);
		quitter.setMinHeight(120.0);

		creer.setOnAction(e -> {
			CreationFestival fen = new CreationFestival();
			fen.show();
			close();
		});
		
		charger.setOnAction(e -> {
			ChargerFestival fen;
			try {
				fen = new ChargerFestival();
				fen.show();
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			}
			close();
		});

		quitter.setOnAction(e -> {
			close();
		});

		return groot;
	}

}
