package logiciels;

import java.io.Serializable;

public abstract class ElementStand implements Serializable {
	private String nom;
	private float prix;
	private int quantite;
	public ElementStand(String n,float p,int q)
	{
		this.nom=n;
		this.prix=p;
		this.quantite=q;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public float getPrix() {
		return prix;
	}
	public void setPrix(float prix) {
		this.prix = prix;
	}
	public int getQuantite() {
		return quantite;
	}
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	public abstract float calculCout();
	
}
