package logiciels;

import java.util.ArrayList;

public class StandPreventionSecours extends Stand {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2348313969620262058L;
	private int nbPers;
	private float coutPers;
	private ArrayList<ElementPreventionSecours> listeElementsPreventionSecours;

	// CONSTRUCTEUR
	public StandPreventionSecours(String n, String taille, float hauteur, float longueur, float largeur) {
		super(n, taille, hauteur, longueur, largeur);
		this.nbPers = 0;
		this.coutPers = 0;
		this.listeElementsPreventionSecours = new ArrayList<ElementPreventionSecours>();
	}

	// GETTERS ET SETTERS
	public float getCoutPers() {
		return coutPers;
	}

	public void setCoutPers(float coutPers) {
		this.coutPers = coutPers;
	}
	

	public ArrayList<ElementPreventionSecours> getListeElementsPreventionSecours() {
		return listeElementsPreventionSecours;
	}

	public void setListeElementsPreventionSecours(ArrayList<ElementPreventionSecours> listeElementsPreventionSecours) {
		this.listeElementsPreventionSecours = listeElementsPreventionSecours;
	}

	// AJOUTER UN ARTICLE DE PREVENTION AU STAND
	public void ajoutElementPreventionSecour(ElementPreventionSecours p) {
		listeElementsPreventionSecours.add(p);
		calculCout();
	}

	// MODIFIER UN ARTICLE DE PREVENTION DU STAND
	public void editerElementPreventionSecour(ElementPreventionSecours p) {
		int i = 0;
		boolean trouve = false;
		while ((i <= listeElementsPreventionSecours.size()) && (trouve == false)) {
			if (listeElementsPreventionSecours.get(i).equals(p)) {
				trouve = true;
			} else {
				i++;
			}
		}
		if (trouve) {
			listeElementsPreventionSecours.set(i, p);
		} else {
			System.out.println("Erreur : l'article de pr�vention " + p + " n'existe pas dans la liste");
		}
	}

	// SUPPRIMMER UN ARTICLE DE PREVENTION DU STAND
	public void supprimmerElementPreventionSecour(ElementPreventionSecours p) {
		if (listeElementsPreventionSecours.contains(p)) {
			listeElementsPreventionSecours.remove(p);
		} else {
			System.out.println("Erreur : l'article de pr�vention" + p + " n'existe pas dans la liste");
		}
	}

	// METHODE DE CALCUL DU COUT
	@Override
	public float calculCout() {
		float coutTotal = 0;

		if (listeElementsPreventionSecours.isEmpty()) {
			System.out.println("Erreur : pas d'article de pr�vention assign� � ce stand");
		} else {
			for (int i = 0; i < listeElementsPreventionSecours.size(); i++) {
				coutTotal += listeElementsPreventionSecours.get(i).calculCout();
			}
		}
		cout += coutTotal + nbPers * coutPers;
		return cout;
	}

	// METHODES AFFICHAGE
	public String listerElementsPreventionSecours() {
		String s = new String("\n - contenu du stand :\n");
		if (listeElementsPreventionSecours.size() == 0) {
			s += "	> vide";
		} else {
			for (int i = 0; i < listeElementsPreventionSecours.size(); i++) {
				s += ("	> " + listeElementsPreventionSecours.get(i) + "\n");
			}
		}
		return s;
	}

	@Override
	public String toString() {
		return super.toString() + this.listerElementsPreventionSecours();
	}

	// METHODE OBTENTION DU NOM DE LA CLASSE
	@Override
	public String getNomClasse() {
		return "Stand Pr�vention Secours";
	}

}
