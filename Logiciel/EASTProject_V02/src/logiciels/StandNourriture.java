package logiciels;

import java.util.ArrayList;

public class StandNourriture extends Stand {
	private ArrayList<ElementNourriture> ListeElementsNourritures;

	public StandNourriture(String n, String taille, float hauteur, float longueur, float largeur) {
		super(n, taille, hauteur, longueur, largeur);
		this.ListeElementsNourritures = new ArrayList<ElementNourriture>();
	}

	// AJOUTER UN ELEMENT DE NOURRITURE AU STAND
	public void ajouElementNourriture(ElementNourriture n) {
		ListeElementsNourritures.add(n);
	}

	// MODIFIER UN ELEMENT DE NOURRITURE DU STAND
	public void editerElementNourriture(ElementNourriture n) {
		int i = 0;
		boolean trouve = false;
		while ((i <= ListeElementsNourritures.size()) && (trouve == false)) {
			if (ListeElementsNourritures.get(i).equals(n)) {
				trouve = true;
			} else {
				i++;
			}
		}
		if (trouve) {
			ListeElementsNourritures.set(i, n);
		} else {
			System.out.println("Erreur : le repas " + n + " n'existe pas dans la liste");
		}
	}

	// SUPPRIMMER UN ELEMENT DE NOURRITURE DU STAND
	public void supprimmerElementNourriture(ElementNourriture n) {
		if (ListeElementsNourritures.contains(n)) {
			ListeElementsNourritures.remove(n);
		} else {
			System.out.println("Erreur : le repas " + n + "n'existe pas dans la liste");
		}
	}

	// METHODE DE CALCUL DU COUT
	@Override
	public float calculCout() {
		float coutTotal = 0;

		if (ListeElementsNourritures.isEmpty()) {
			System.out.println("Erreur : pas de repas assign� � ce stand");
		} else {
			for (int i = 0; i < ListeElementsNourritures.size(); i++) {
				coutTotal += ListeElementsNourritures.get(i).calculCout();
			}
		}
		return coutTotal;
	}

	// METHODE OBTENTION DU NOM DE LA CLASSE
	@Override
	public String getNomClasse() {
		return "Stand Nourriture";
	}

}
