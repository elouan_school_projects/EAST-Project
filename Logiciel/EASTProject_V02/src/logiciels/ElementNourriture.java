package logiciels;

public class ElementNourriture extends ElementStand {


	// CONSTRUCTEUR
	public ElementNourriture(String nom, float prixElementNourriture, int quantite) {
		super(nom,prixElementNourriture,quantite);
	}

	// METHODE DE CALCUL DU COUT
	public float calculCout() {
		return this.getQuantite()*this.getPrix();
	}
	public String toString()
	{
		return this.getNom()+" "+this.getPrix()+" "+this.getQuantite();
	}
}
