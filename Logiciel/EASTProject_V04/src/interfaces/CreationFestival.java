package interfaces;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import logiciels.ProjetFestival;

public class CreationFestival extends Stage {

	private Button creer = new Button("Creer le festival");
	private Button annuler = new Button("Annuler la cr�ation");
	private Label nom = new Label("Nom : ");
	private Label dateD = new Label("Date de d�but : ");
	private Label dateF = new Label("Date de fin : ");
	private Label budget = new Label("Budget initial : ");
	private Label taille = new Label("Taille du festival : ");
	private Label emplacement = new Label("Emplacement du festival : ");
	private TextField saisieN = new TextField();
	private DatePicker saisieD = new DatePicker();
	private DatePicker saisieF = new DatePicker();
	private TextField saisieB = new TextField();
	private TextField saisieT = new TextField();
	private TextField saisieE = new TextField();

	public CreationFestival() {
		this.setTitle("Creer un festival");
		this.setX(520);
		this.setY(300);
		this.setResizable(false);
		this.initModality(Modality.APPLICATION_MODAL);

		Scene laScene = new Scene(affichage(), 600, 365);
		this.setScene(laScene);
		this.sizeToScene();
	}

	@SuppressWarnings("deprecation")
	Parent affichage() {
		// essai avec gridpane
		GridPane groot = new GridPane();

		groot.add(nom, 2, 1);
		groot.add(dateD, 2, 2);
		groot.add(dateF, 2, 3);
		groot.add(budget, 2, 4);
		groot.add(taille, 2, 5);
		groot.add(emplacement, 2, 6);
		groot.add(saisieN, 3, 1);
		groot.add(saisieD, 3, 2);
		groot.add(saisieF, 3, 3);
		groot.add(saisieB, 3, 4);
		groot.add(saisieT, 3, 5);
		groot.add(saisieE, 3, 6);
		groot.add(creer, 2, 8);
		groot.add(annuler, 3, 8);

		nom.setFont(new Font("Arial", 22));
		dateD.setFont(new Font("Arial", 22));
		dateF.setFont(new Font("Arial", 22));
		budget.setFont(new Font("Arial", 22));
		taille.setFont(new Font("Arial", 22));
		emplacement.setFont(new Font("Arial", 22));
		creer.setFont(new Font("Arial", 18));
		annuler.setFont(new Font("Arial", 18));

		groot.setHgap(20);
		groot.setVgap(15);

		creer.setMinWidth(250.0);
		creer.setMinHeight(70.0);
		annuler.setMinWidth(250.0);
		annuler.setMinHeight(70.0);

		saisieN.setPromptText("Exemple");
		saisieD.setPromptText("DD/MM/YYYY");
		saisieF.setPromptText("DD/MM/YYYY");
		saisieB.setPromptText("1000�");
		saisieT.setPromptText("En m�tres");
		saisieE.setPromptText("Carhaix");

		annuler.setOnAction(e -> {
			if (FenetrePrincipale.impl_getWindows() != null) {
				close();
			} else {
				MenuPrincipal fen = new MenuPrincipal();
				fen.show();
				close();
			}

		});

		creer.setOnAction(e -> {

			if (isInputValid()) {
				MenuPrincipal.projet = new ProjetFestival(saisieN.getText(), saisieD.getValue(), saisieF.getValue(),
						Float.parseFloat(saisieB.getText()), Float.parseFloat(saisieT.getText()), saisieE.getText(),
						"sauvegardes/" + saisieN.getText() + ".ser");
				FenetrePrincipale fen = new FenetrePrincipale();
				fen.show();
				close();

			}
		});

		entierRelatif(saisieB);
		entierRelatif(saisieT);

		return groot;
	}

	public Button getCreer() {
		return creer;
	}

	private boolean isInputValid() {
		Stage dialogStage = null;
		String msgErreur = "";

		if (saisieN.getText() == null || saisieN.getText().length() == 0) {
			msgErreur += "Veuillez saisir un nom de festival ! \n";
		}
		if (isNumeric(saisieN.getText())) {
			msgErreur += "Le nom ne peut pas �tre compos� que de chiffres !\n";
		}

		if (saisieD.getValue() == null) {
			msgErreur += "Veuillez saisir une date de d�but ! \n";
		}
		if (saisieF.getValue() == null) {
			msgErreur += "Veuillez saisir une date de fin ! \n";
		}
		if (saisieD.getValue() != null || saisieF.getValue() != null) {
			if (!isPossible()) {
				msgErreur += "L'intervalle demand� n'est pas possible !\n";
			}
		}

		if (saisieB.getText() == null || saisieB.getText().length() == 0) {
			msgErreur += "Veuillez saisir un budget initial !\n";
		}

		if (saisieT.getText() == null || saisieT.getText().length() == 0) {
			msgErreur += "Veuillez saisir une taille � votre festival !\n";
		}

		if (saisieE.getText() == null || saisieE.getText().length() == 0) {
			msgErreur += "Veuillez saisir l'emplacement du festival !\n";
		}
		if (isNumeric(saisieE.getText())) {
			msgErreur += "Un nom de lieu ne peut �tre compos� que de lettres !\n";
		}

		if (msgErreur.length() == 0) {
			return true;
		} else {
			// Affiche le message d'erreur
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(dialogStage);
			alert.setTitle("Champs incorrects");
			alert.setHeaderText("Veuillez corriger les champs suivants");
			alert.setContentText(msgErreur);

			alert.showAndWait();

			return false;
		}
	}

	private boolean isNumeric(String str) {
		// renvoie true si la chaine str correspond � un nombre, �ventuellement n�gatif
		// et �ventuellement � virgule
		return str.matches("-?\\d+(\\.\\d+)?");
	}

	private boolean isPossible() {

		if (saisieD.getValue().equals(saisieF.getValue())) {
			return false;
		} else if (saisieD.getValue().isAfter(saisieF.getValue())) {
			return false;
		}

		return true;
	}

	public void entierRelatif(TextField tf) {
		tf.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("[\\d.]*")) {
					tf.setText(newValue.replaceAll("[^\\d.]", ""));
				}
			}
		});
	}
}
