package logiciels;

import java.time.LocalDate;

public class ProjetFestival {
	private String nom;
	private LocalDate dateDeb;
	private LocalDate dateFin;
	private double budget;
	static private Carte carte;

	// CONSTRUCTEUR

	public ProjetFestival(String nom, LocalDate dateDeb, LocalDate dateFin, double budget) {
		this.nom = nom;
		this.dateDeb = dateDeb;
		this.dateFin = dateFin;
		this.budget = budget;
		ProjetFestival.carte = new Carte(0, "Inconnue");
	}

	// GETTERS ET SETTERS

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public LocalDate getDateDeb() {
		return dateDeb;
	}

	public void setDateDeb(LocalDate dateDeb) {
		this.dateDeb = dateDeb;
	}

	public LocalDate getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}

	public double getBudget() {
		return budget;
	}

	public void setBudget(double budget) {
		this.budget = budget;
	}

	public static void setCarte(Carte carte) {
		ProjetFestival.carte = carte;
	}

	public static Carte getCarte() {
		return ProjetFestival.carte;
	}

}
