package logiciels;

import java.util.ArrayList;

public class StandNourriture extends Stand {
	private ArrayList<ElementNourriture> listeElementsNourritures;

	public StandNourriture(String n, String taille, float hauteur, float longueur, float largeur) {
		super(n, taille, hauteur, longueur, largeur);
		this.listeElementsNourritures = new ArrayList<ElementNourriture>();
	}

	public ArrayList<ElementNourriture> getListeElementsNourritures() {
		return listeElementsNourritures;
	}

	public void setListeElementsNourritures(ArrayList<ElementNourriture> listeElementsNourritures) {
		this.listeElementsNourritures = listeElementsNourritures;
	}

	// AJOUTER UN ELEMENT DE NOURRITURE AU STAND
	public void ajouElementNourriture(ElementNourriture n) {
		listeElementsNourritures.add(n);
		calculCout();
	}

	// MODIFIER UN ELEMENT DE NOURRITURE DU STAND
	public void editerElementNourriture(ElementNourriture n) {
		int i = 0;
		boolean trouve = false;
		while ((i <= listeElementsNourritures.size()) && (trouve == false)) {
			if (listeElementsNourritures.get(i).equals(n)) {
				trouve = true;
			} else {
				i++;
			}
		}
		if (trouve) {
			listeElementsNourritures.set(i, n);
		} else {
			System.out.println("Erreur : le repas " + n + " n'existe pas dans la liste");
		}
	}

	// SUPPRIMMER UN ELEMENT DE NOURRITURE DU STAND
	public void supprimmerElementNourriture(ElementNourriture n) {
		if (listeElementsNourritures.contains(n)) {
			listeElementsNourritures.remove(n);
		} else {
			System.out.println("Erreur : le repas " + n + "n'existe pas dans la liste");
		}
	}

	// METHODE DE CALCUL DU COUT
	@Override
	public float calculCout() {
		float coutTotal = 0;

		if (listeElementsNourritures.isEmpty()) {
			System.out.println("Erreur : pas de repas assign� � ce stand");
		} else {
			for (int i = 0; i < listeElementsNourritures.size(); i++) {
				coutTotal += listeElementsNourritures.get(i).calculCout();
			}
		}
		cout += coutTotal;
		return cout;
	}

	// METHODES AFFICHAGE
	public String listerElementsNourriture() {
		String s = new String("\n - contenu du stand :\n");
		if (listeElementsNourritures.size() == 0) {
			s += "	> vide";
		} else {
			for (int i = 0; i < listeElementsNourritures.size(); i++) {
				s += ("	> " + listeElementsNourritures.get(i) + "\n");
			}
		}
		return s;
	}

	@Override
	public String toString() {
		return super.toString() + this.listerElementsNourriture();
	}

	// METHODE OBTENTION DU NOM DE LA CLASSE
	@Override
	public String getNomClasse() {
		return "Stand Nourriture";
	}

}
