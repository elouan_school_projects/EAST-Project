package provisoire;

public class Devis {
	private String titre;
	private float prix;

	// Constructeur
	public Devis(String titre, float prix) {
		this.titre = titre;
		this.prix = prix;
	}

	// GETTERS ET SETTERS
	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public float getPrix() {
		return prix;
	}

	public void setPrix(float prix) {
		this.prix = prix;
	}

}
