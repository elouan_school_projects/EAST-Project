package logiciels;

import javafx.scene.image.Image;

public enum ListeImages {
	PETITSTANDBOISSON("/PetitStandBoisson.png", 31, 31),
	MOYENSTANDBOISSON("/MoyenStandBoisson.png", 62, 31),
	GRANDSTANDBOISSON("/GrandStandBoisson.png", 77, 46),
	PETITSTANDBOUTIQUE("/PetitStandBoutique.png", 31, 31),
	MOYENSTANDBOUTIQUE("/MoyenStandBoutique.png", 62, 31),
	GRANDSTANDBOUTIQUE("/GrandStandBoutique.png", 77, 46),
	PETITSTANDCONVERSION("/PetitStandConversion.png", 31, 31),
	MOYENSTANDCONVERSION("/MoyenStandConversion.png", 62, 31),
	GRANDSTANDCONVERSION("/GrandStandConversion.png", 77, 46),
	PETITSTANDNOURRITURE("/PetitStandNourriture.png", 31, 31),
	MOYENSTANDNOURRITURE("/MoyenStandNourriture.png", 62, 31),
	GRANDSTANDNOURRITURE("/GrandStandNourriture.png", 77, 46),
	PETITSTANDPREVENTIONSECOURS("/PetitStandPreventionSecours.png", 31, 31),
	MOYENSTANDPREVENTIONSECOURS("/MoyenStandPreventionSecours.png", 62, 31),
	GRANDSTANDPREVENTIONSECOURS("/GrandStandPreventionSecours.png", 77, 46),
	PETITSTANDSANITAIRE("/PetitStandSanitaire.png", 31, 31),
	MOYENSTANDSANITAIRE("/MoyenStandSanitaire.png", 62, 31),
	GRANDSTANDSANITAIRE("/GrandStandSanitaire.png", 77, 46),
	PETITSTANDSCENE("/PetitStandScene.png", 31, 31),
	MOYENSTANDSCENE("/MoyenStandScene.png", 62, 31),
	GRANDSTANDSCENE("/GrandStandScene.png", 77, 46);
	
	private Image image;
	private String url;
	private float largeur;
	private float longueur;

	private ListeImages(String url, float largeur, float longueur) {
		this.image = new Image(getClass().getResource(url).toExternalForm() ,largeur, longueur, false, false);
		this.url = url;
		this.largeur = largeur;
		this.longueur = longueur;
	}

	public Image getImage() {
		return image;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public float getLargeur() {
		return largeur;
	}

	public void setLargeur(float largeur) {
		this.largeur = largeur;
	}

	public float getLongueur() {
		return longueur;
	}

}