package logiciels;

import java.io.Serializable;

public class Artiste implements Serializable{
	private String nom;
	private double cachet;
	private String genre;

	// CONSTRUCTEUR
	public Artiste(String nom, double cachet, String genre) {
		this.nom = nom;
		this.cachet = cachet;
		this.genre = genre;
	}

	// Getters et Setters
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public double getCachet() {
		return cachet;
	}

	public void setCachet(float cachet) {
		this.cachet = cachet;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public double calculCout() {
		return cachet;
	}

	@Override
	public String toString() {
		return "nom : " + nom + "\ncachet : " + cachet + "\ngenre : " + genre;
	}
}
