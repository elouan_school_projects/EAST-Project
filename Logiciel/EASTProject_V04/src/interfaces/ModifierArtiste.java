package interfaces;

import java.time.LocalTime;
import java.time.chrono.ChronoLocalDate;
import java.util.ArrayList;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import logiciels.Artiste;
import logiciels.ProjetFestival;
import logiciels.Representation;
import logiciels.StandScene;

public class ModifierArtiste extends Stage {

	private GridPane saisie = new GridPane();
	private VBox labelsGauche = new VBox();
	private GridPane creerScene = new GridPane();
	private ListView<Representation> listeRepresentation;
	private HBox boutonsBot = new HBox();
	private VBox poub = new VBox();
	private HBox imageTitre = new HBox();
	private TextField heures = new TextField();
	private TextField minutes = new TextField();
	private Label Splithoraire = new Label(" : ");
	private HBox horaire = new HBox();

	private Label nom = new Label("Nom : ");
	private Label cachet = new Label("Cachet : ");
	private Label genre = new Label("Genre : ");
	private Label scene = new Label("Scenes : ");
	private Label listeScenes = new Label("Liste des sc�nes de l'Artiste");

	private TextField nomS = new TextField();
	private TextField cachetS = new TextField();
	private TextField genreS = new TextField();
	private TextField duree = new TextField();
	private DatePicker date = new DatePicker();
	private ComboBox<String> scenesDispo;

	private Button ajouter = new Button("Ajouter");
	private Button creer = new Button("Creer");
	private Button annuler = new Button("Annuler");
	private ProjetFestival pf;
	private static Artiste a;
	private static ArrayList<StandScene> listeS = new ArrayList<StandScene>();

	public ModifierArtiste(ProjetFestival f, ArrayList<StandScene> lscene) {
		this.setTitle("Creer une representation");
		this.setResizable(false);
		pf = f;
		listeS = lscene;
		Scene laScene = new Scene(affichage(), 756, 400);
		this.setScene(laScene);
		this.sizeToScene();
	}

	public Parent affichage() {
		// CREATION DES LISTES
		ObservableList<Representation> Rep = FXCollections.observableArrayList(

		);
		ObservableList<String> Scenes = FXCollections.observableArrayList(

		);
		scenesDispo = new ComboBox<String>(Scenes);
		scenesDispo.setPromptText("Selectionnez une Scene");
		listeRepresentation = new ListView<Representation>(Rep);
		// EMPECHE LA CREATION SI CHAMPS NON SAISIES

		ajouter.disableProperty()
				.bind(minutes.textProperty().isEmpty().or(date.valueProperty().isNull())
						.or(heures.textProperty().isEmpty()).or(duree.textProperty().isEmpty())
						.or(cachetS.textProperty().isEmpty()).or(nomS.textProperty().isEmpty())
						.or(genreS.textProperty().isEmpty()));

		creer.disableProperty().bind(
				cachetS.textProperty().isEmpty().or(nomS.textProperty().isEmpty()).or(genreS.textProperty().isEmpty()));

		// DEFINITION DES REGLES DE SAISIE POUR L'HEURE
		SaisieHoraire(heures, 23);
		SaisieHoraire(minutes, 59);

		// HORAIRE
		heures.setMaxWidth(80);
		minutes.setMaxWidth(80);
		heures.setPromptText("Heures");
		minutes.setPromptText("minutes");
		horaire.getChildren().addAll(heures, Splithoraire, minutes);
		// LISTE DES SCENES DISPO

		// RECHERCHE DES STANDS SCENE DISPO DANS LA CARTE
		for (int i = 0; i < pf.getCarte().getStand().size(); i++) {
			if (pf.getCarte().getStand().get(i).getNomClasse().equals("Stand Scene")) {
				Scenes.add(pf.getCarte().getStand().get(i).getNom());
				listeS.add((StandScene) pf.getCarte().getStand().get(i));
			}
		}
		date.setEditable(false);
		date.setPromptText("Date");
		VBox root = new VBox();
		// Labels a gauche

		labelsGauche.getChildren().addAll(nom, cachet, genre, scene);
		labelsGauche.setSpacing(25);
		saisie.add(labelsGauche, 0, 0);

		// textfields droite
		creerScene.add(nomS, 0, 0, 3, 1);
		creerScene.add(cachetS, 0, 1, 3, 1);
		creerScene.add(genreS, 0, 2, 3, 1);
		creerScene.add(scenesDispo, 0, 3, 3, 1);
		creerScene.add(horaire, 0, 4);
		creerScene.add(duree, 1, 4);
		creerScene.add(date, 2, 4);
		creerScene.add(ajouter, 3, 3, 1, 2);
		creerScene.setHgap(10);
		creerScene.setVgap(15);
		creerScene.setGridLinesVisible(false);

		duree.setPromptText("Duree (Minutes)");

		saisie.add(creerScene, 1, 0);
		saisie.setHgap(90);
		scenesDispo.setMaxWidth(Double.MAX_VALUE);

		// listview scenes
		listeRepresentation.setPrefHeight(90);

		// Boutons Bot
		boutonsBot.getChildren().addAll(creer, annuler);
		boutonsBot.setAlignment(Pos.BASELINE_CENTER);
		boutonsBot.setSpacing(60);

		creer.setMaxWidth(200);
		annuler.setMaxWidth(200);
		HBox.setHgrow(creer, Priority.ALWAYS);
		HBox.setHgrow(annuler, Priority.ALWAYS);

		String url = "https://image.ibb.co/k6EqO8/54158.png";
		Image image = new Image(url);
		ImageView imageView = new ImageView(image);
		poub.setSpacing(2);

		// HBox pour le titre de la liste et l'image de poubelle
		imageTitre.getChildren().addAll(imageView, listeScenes);
		imageTitre.setSpacing(250);

		poub.getChildren().addAll(imageTitre, listeRepresentation);

		root.setSpacing(20);
		root.getChildren().addAll(saisie, poub, boutonsBot);
		root.setPadding(new Insets(20));

		// GESTION DE LA LISTVIEW pour enlever un item selectionner
		imageView.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent arg0) {
				final int selectedIdx = listeRepresentation.getSelectionModel().getSelectedIndex();
				if (selectedIdx != -1) {

					final int newSelectedIdx = (selectedIdx == listeRepresentation.getItems().size() - 1)
							? selectedIdx - 1
							: selectedIdx;

					for (int k = 0; k < listeS.size(); k++) {
						for (int z = 0; z < listeS.get(k).getListerep().size(); z++) {
							if (listeS.get(k).getListerep().get(z)
									.equals(listeRepresentation.getItems().get(selectedIdx))) {
								listeS.get(k).enlever(listeRepresentation.getItems().get(selectedIdx));
								listeRepresentation.getItems().remove(selectedIdx);
							}
						}

					}
					listeRepresentation.getSelectionModel().select(newSelectedIdx);
				}
			}
		});
		// CREER LA REPRESENTATION
		creer.setOnAction(e -> {

			a = new Artiste(nomS.getText(), Double.parseDouble(cachetS.getText()), genreS.getText());

			this.close();

		});
		this.setOnCloseRequest(event -> {
			if (!event.getSource().equals(annuler) && !event.getSource().equals(creer)) {
				listeS.clear();
				a = null;
			}

		});
		// FERME LA FENETRE
		annuler.setOnAction(e -> {
			listeS.clear();
			a = null;
			this.close();

		});
		// AJOUTE A LA LISTE DES SCENES DE LA FENETRE
		ajouter.setOnAction(e -> {

			if (date.getValue().isEqual((ChronoLocalDate) pf.getDateDeb())
					|| date.getValue().isAfter((ChronoLocalDate) pf.getDateDeb())
					|| date.getValue().isBefore((ChronoLocalDate) pf.getDateFin())
					|| date.getValue().isEqual((ChronoLocalDate) pf.getDateFin())) {
				for (int j = 0; j < listeS.size(); j++) {
					if (listeS.get(j).getNom().equals(scenesDispo.getSelectionModel().getSelectedItem())) {

						Representation r = new Representation(date.getValue(),
								LocalTime.of(Integer.parseInt(heures.getText()), Integer.parseInt(minutes.getText())),
								new Artiste(nomS.getText(), Double.parseDouble(cachetS.getText()), genreS.getText()),
								Long.parseLong(duree.getText()));
						if (!listeS.get(j).getListerep().contains(r)) {
							listeS.get(j).ajouterRep(r);
							listeRepresentation.getItems().add(r);
						} else {
							System.out.println("Ne peut pas ajouter");
						}

					}
				}
			} else {
				Stage dialogStage = null;
				Alert alert = new Alert(AlertType.ERROR);
				alert.initOwner(dialogStage);
				alert.setTitle("Champs incorrects");
				alert.setHeaderText("Saisie Incorrect");
				alert.setContentText("Veuillez saisir une date dans l'intervalle donn� a la cr�ation du festival");

				alert.showAndWait();
			}

		});
		Entier(duree);
		EntierRelatif(cachetS);
		// VERIFICATION SI LA LISTE DE REPRESENTAION EST VIDE

		return root;
	}

	// EMPECHE DE SAISIR DES INTERVALLES DE TEMPS INCORECT
	public void SaisieHoraire(TextField tf, int fin) {
		tf.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("[\\d]*")) {
					tf.setText(newValue.replaceAll("[^\\d]", ""));
				} else if (newValue.length() != 0) {
					if (newValue.matches("0[0-9]")) {
						tf.setText("0");
					} else if (Integer.parseInt(newValue) > fin) {

						tf.setText(String.valueOf(fin));
					}
				}

			}
		});
	}

	// AUTORISE QUE LA SAISIE D'ENTIER
	public void Entier(TextField tf) {
		tf.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("[\\d]*")) {
					tf.setText(newValue.replaceAll("[^\\d]", ""));
				}
			}
		});
	}

	// AUTORISE LA SAISIE D'ENTIER RELATIF
	public void EntierRelatif(TextField tf) {
		tf.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("[\\d.]*")) {
					tf.setText(newValue.replaceAll("[^\\d.]", ""));
				}
			}
		});
	}

	public static ArrayList<StandScene> getListeS() {
		return listeS;
	}

	public static void setListeS(ArrayList<StandScene> listeS) {
		ModifierArtiste.listeS = listeS;
	}

}
