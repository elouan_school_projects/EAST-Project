package logiciels;

import java.util.ArrayList;

public class StandBoisson extends Stand {
	private ArrayList<ElementBoisson> listeElementsBoisson;

	public StandBoisson(String n, String taille, float hauteur, float longueur, float largeur) {
		super(n, taille, hauteur, longueur, largeur);
		this.listeElementsBoisson = new ArrayList<ElementBoisson>();

	}

	public ArrayList<ElementBoisson> getListeElementsBoisson() {
		return listeElementsBoisson;
	}

	public void setListeElementsBoisson(ArrayList<ElementBoisson> listeElementsBoisson) {
		this.listeElementsBoisson = listeElementsBoisson;
	}

	// AJOUTER UNE BOISSON AU STAND
	public void ajoutElementBoisson(ElementBoisson b) {
		listeElementsBoisson.add(b);
		calculCout();
	}

	// MODIFIER UNE BOISSON DU STAND
	public void editerElementBoisson(ElementBoisson b) {
		int i = 0;
		boolean trouve = false;
		while ((i <= listeElementsBoisson.size()) && (trouve == false)) {
			if (listeElementsBoisson.get(i).equals(b)) {
				trouve = true;
			} else {
				i++;
			}
		}
		if (trouve) {
			listeElementsBoisson.set(i, b);
		} else {
			System.out.println("Erreur : la boisson " + b + "n'existe pas dans la liste");
		}
	}

	// SUPPRIMMER UNE BOISSON DU STAND
	public void supprimmerElementBoisson(ElementBoisson b) {
		if (listeElementsBoisson.contains(b)) {
			listeElementsBoisson.remove(b);
		} else {
			System.out.println("Erreur : la boisson " + b + "n'existe pas dans la liste");
		}
	}

	// METHODE DE CALCUL DU COUT
	@Override
	public float calculCout() {
		float coutTotal = 0;

		if (listeElementsBoisson.isEmpty()) {
			System.out.println("Erreur : pas de boissons assign�e � ce stand");
		} else {
			for (int i = 0; i < listeElementsBoisson.size(); i++) {
				coutTotal += listeElementsBoisson.get(i).calculCout();
			}
		}
		cout += coutTotal;
		return cout;
	}

	// METHODES AFFICHAGE
	public String listerElementsBoisson() {
		String s = new String("\n - contenu du stand :\n");
		if (listeElementsBoisson.size() == 0) {
			s += "	> vide";
		} else {
			for (int i = 0; i < listeElementsBoisson.size(); i++) {
				s += ("	> " + listeElementsBoisson.get(i) + "\n");
			}
		}
		return s;
	}

	@Override
	public String toString() {
		return super.toString() + this.listerElementsBoisson();
	}

	// METHODE OBTENTION DU NOM DE LA CLASSE
	@Override
	public String getNomClasse() {
		return "Stand Boisson";
	}

}
