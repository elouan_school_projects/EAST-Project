package logiciels;


import interfaces.MenuPrincipal;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

	public static void main(String[] args) {
		Application.launch(args);

	}


	public void start(Stage arg0) throws Exception {
		arg0 = new MenuPrincipal();
		arg0.show();
	}
	
}
